package seas2.maxsum;

import seas2.core.Assignment;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.core.BaseSolver;

public class FastMaxSumSolver extends BaseSolver implements Solver {

	public static final String suffix = new String("FastMaxSum");

	public static final String minError = new String("minError" + suffix);
	public static final String maxSteps = new String("maxSteps" + suffix);
	public static final String numSteps = new String("numSteps" + suffix);
	public static final String numMessages = new String("numMessages" + suffix);
	public static final String numTransactions = new String("numTransactions"
			+ suffix);
	public static final String solution = new String("solution" + suffix);
	public static final String eval = new String("eval" + suffix);
	public static final String time = new String("time" + suffix);
	public static final double DEFAULT_MIN_ERROR = 1e-6;
	public static final int DEFAULT_MAX_STEPS = 1000;

	@Override
	public Options solve(Problem problem, Options m) {
		double minErrorD = DEFAULT_MIN_ERROR;
		int maxStepsI = DEFAULT_MAX_STEPS;
		if (m.containsKey(minError))
			minErrorD = (Double) m.get(minError);
		if (m.containsKey(maxSteps))
			maxStepsI = (Integer) m.get(maxSteps);
		FactorGraph fg = new FastFactorGraph(problem);
		fg.setMaxSteps(maxStepsI);
		fg.setMinError(minErrorD);
		Assignment a = fg.solve();
		Options results = new Options();
		results.put(Solver.solution, a);
		results.put(FastMaxSumSolver.solution, a);
		results.put(FastMaxSumSolver.numSteps, fg.getNumSteps());
		results.put(FastMaxSumSolver.numMessages, fg.getNumMessages());
		results.put(FastMaxSumSolver.numTransactions, fg.getNumTransactions());
		return results;
	}
}
