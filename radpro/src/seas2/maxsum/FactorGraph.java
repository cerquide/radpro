package seas2.maxsum;

import java.util.HashMap;
import java.util.Map;

import seas2.core.Assignment;
import seas2.core.Link;
import seas2.core.OneWay;
import seas2.core.Participant;
import seas2.core.Problem;

public class FactorGraph {

	public final static double NOISE = 1e-6;

	private Map<Long, ProsumerFactor> prosumerFactors = new HashMap<Long, ProsumerFactor>();
	private Problem problem;
	private int maxSteps = MaxSumSolver.DEFAULT_MAX_STEPS;
	private double minError = MaxSumSolver.DEFAULT_MIN_ERROR;
	private int numSteps;
	private int numMessages = 0;
	private int numTransactions = 0;

	public FactorGraph(Problem problem) {
		this.problem = problem;
		init();
	}

	protected ProsumerFactor makeParticipantFactor(Participant p) {
		String name = (p instanceof OneWay ? "b" : "p") + p.getId();
		System.out.println("Creating participant " + name);
		MSValuation valuation = new MSValuation(p.getValuation());
		valuation.addNoise(NOISE);
		ProsumerFactor f;
		if (p instanceof OneWay)
			f = new OneWayFactor(name, valuation);
		else
			f = new ProsumerFactor(name, valuation);
		return f;
	}
	
	protected void init() {
		for (Participant p : problem.getParticipants()) {
			// Create the participant factors
			ProsumerFactor f = makeParticipantFactor(p);
			prosumerFactors.put(p.getId(), f);
		}

		for (Link l : problem.getLinks()) {
			long sourceId = l.source.getId();
			long destId = l.dest.getId();
			makeNeighbors(prosumerFactors.get(sourceId),
					prosumerFactors.get(destId), l);
		}
	}

	private void makeNeighbors(ProsumerFactor src, ProsumerFactor dst, Link l) {
		MSValuation msg = new MSValuation();
		src.addOutNeighbor(dst, l);
		src.send(msg, dst);
		numMessages++;
		dst.addInNeighbor(src, l);
		dst.send(msg, src);
		numMessages++;
	}

	private void nextIteration() {
		numSteps++;
		for (MSFactor factor : prosumerFactors.values()) {
			// System.out.println("Factor " + factor.getName()
			// + " receives messages...");
			factor.nextIteration();
		}
	}

	public void run(int steps, double eps) {
		double current_error = Double.MAX_VALUE;
		numSteps = 0;
		nextIteration();
		while ((numSteps < steps) && (current_error > eps)) {
			System.out.println("===========");
			System.out.println("STEP " + numSteps + ":\n");
			runFactors();
			printMarginals();
			nextIteration();
			current_error = assessError();
			System.out.println("Error: " + current_error);
		}
		numSteps--;
	}

	private void runFactors() {
		for (MSFactor factor : prosumerFactors.values()) {
			System.out.println("Factor " + factor.getName() + " is running...");
			factor.run();
			numMessages += factor.getNumNeighbors();
		}
	}

	private double assessError() {
		double error = -Double.MAX_VALUE;
		for (MSFactor f : prosumerFactors.values()) {
			error = Math.max(error, f.assessError());
		}
		return error;
	}

	public void printMarginals() {
		for (MSFactor f : prosumerFactors.values()) {
			printMarginals(f);
		}
	}

	private void printMarginals(MSFactor f) {
		System.out.println("Marginals for factor:" + f.getName());
		for (Map.Entry<Link, MSValuation> e : f.getMarginals().entrySet()) {
			System.out.println("\tFor variable: "
					+ e.getKey().getVariableName() + " is " + e.getValue());
		}
	}

	public Assignment decode() {
		Assignment assignment = new Assignment();
		for (ProsumerFactor f : prosumerFactors.values()) {
			f.decode(assignment);
		}
		return assignment;
	}

	public Assignment solve() {
		run(maxSteps, minError);
		return decode();
	}

	public int getMaxSteps() {
		return maxSteps;
	}

	public void setMaxSteps(int maxSteps) {
		this.maxSteps = maxSteps;
	}

	public double getMinError() {
		return minError;
	}

	public void setMinError(double minEps) {
		this.minError = minEps;
	}

	public int getNumSteps() {
		return numSteps;
	}

	public int getNumMessages() {
		return numMessages;
	}

	public int getNumTransactions() {
		return numTransactions;
	}
	
	public MSFactor getFactor(long id) {
		return prosumerFactors.get(id);
	}
}
