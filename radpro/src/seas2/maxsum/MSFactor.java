package seas2.maxsum;

import java.util.HashMap;
import java.util.Map;

import seas2.core.Link;

public abstract class MSFactor extends Factor {
	private Map<MSFactor, MSValuation> previousIterationMessages = new HashMap<MSFactor, MSValuation>();
	private Map<MSFactor, MSValuation> messages = new HashMap<MSFactor, MSValuation>();
	private Map<MSFactor, MSValuation> nextIterationMessages = new HashMap<MSFactor, MSValuation>();
	
	public MSValuation getMessage(MSFactor f) {
		return messages.get(f);
	}
	
	public void send(MSValuation msg, MSFactor to) {
		to.receive(this,msg);
	}

	private void receive(MSFactor from,MSValuation msg) {
		nextIterationMessages.put(from, msg);
	}

	public abstract void run();

	public double assessError() {
		double error;
		if (previousIterationMessages.size() > 0) {
			error = -Double.MAX_VALUE;
			for (Map.Entry<MSFactor, MSValuation> m : messages.entrySet()) {
				MSValuation prevBelief = previousIterationMessages.get(m.getKey());
				error = Math.max(error,
						prevBelief.distance(m.getValue()));
			}
		} else {
			error = Double.MAX_VALUE;
		}
		return error;
	}

	public void nextIteration() {
		previousIterationMessages = messages;
		messages = nextIterationMessages;
		nextIterationMessages = new HashMap<MSFactor, MSValuation>(messages);
	}
	
	public Map<Link,MSValuation> getMarginals() {
		HashMap<Link,MSValuation> margs = new HashMap<Link,MSValuation>();
		for (Factor f : getNeighbors()) {
			Link l = getLink(f);
			//System.out.println("Marginal for " + l.getVariableName() + " is " + marg);
			margs.put(l, getMarginal((MSFactor)f));
		}
		return margs;
	}

	public abstract MSValuation getMarginal(MSFactor f);
	
}
