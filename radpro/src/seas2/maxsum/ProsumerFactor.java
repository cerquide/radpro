package seas2.maxsum;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import seas2.core.Assignment;
import seas2.core.IndexIterator;
import seas2.core.Link;

/**
 * Factor that represents a prosumer
 *  */
public  class ProsumerFactor extends MSFactor {
	
	protected MSValuation offer;
	
	public ProsumerFactor(String name, MSValuation offer) {
		setName(name);
		this.offer = offer;
	}
	
	public MSValuation getMarginal(MSFactor neighbor) {
		//System.out.println(getName() + " assessing marg for neighbor " + neighbor.getName());

		MSValuation marg = assessMessage(neighbor);
		marg = marg.sumBeliefs(getMessage(neighbor));
		return marg;
	}
	
	public MSValuation assessMessage(MSFactor neighbor) {
		List<MSValuation> ml = new ArrayList<MSValuation>(getNumNeighbors()-1);
		addInMessagesButThisOne(ml, neighbor);
		int numInNeighbors = ml.size();
		addOutMessagesButThisOne(ml, neighbor);
		List<Set<Integer>> domains = new ArrayList<Set<Integer>>();
		for (MSValuation v: ml) {
			domains.add(v.keySet());
		}
		int capacity  = getLink(neighbor).capacity;
		//List<MSValuation> ml =  getAllMessagesButThisOne(neighbor);
		MSValuation msg = new MSValuation();
		for (int k =-capacity; k <= capacity ; k++) {
			IndexIterator indexIterator = new IndexIterator(domains);
			List<Integer> index;
			int s;
			double value;
			int x_i;
			while (indexIterator.hasNext()) {
				index = indexIterator.next();
				//System.out.println(index);
				s = 0;
				value = 0.0;
				for (int i = 0; i < numInNeighbors ; i++) {
					x_i = index.get(i);
					s -= x_i;
					value += ml.get(i).get(x_i);
				}
				for (int i = numInNeighbors; i < index.size() ; i++) {
					x_i = index.get(i);
					s += x_i;
					value += ml.get(i).get(x_i);
				}
				if (offer.containsKey(k-s)) {
					value += offer.get(k-s);
					//System.out.println("Considering pair "+k+","+value);
					msg.consider(k, value);
				}
			}
		}
		if (getOutNeighbors().contains(neighbor)) {
			msg = msg.negate();
		}
		//msg.filter(getLink(neighbor).capacity);
		return msg;
	}
	
	public void run() {
		for (Factor neighbor : getNeighbors()) {
			MSValuation msg = assessMessage((MSFactor)neighbor);
			System.out.println("Sending message to factor "    
					+ neighbor.getName() + " : "
					+ msg);
			send(msg, (MSFactor)neighbor);
		}
	}

	private void addInMessagesButThisOne(List<MSValuation> ml, MSFactor thisOne) {
		for (Factor f: getInNeighbors()) {
			if (f != thisOne) {
				ml.add(getMessage((MSFactor)f));
			}
		}
	}

	private void addOutMessagesButThisOne(List<MSValuation> ml, MSFactor thisOne) {
		for (Factor f: getOutNeighbors()) {
			if (f != thisOne) {
				ml.add(getMessage((MSFactor)f));
			}
		}
	}

	public void decode(Assignment assignment) {
		for (Factor f: getNeighbors()) {
			Link l = getLink(f);
			assignment.put(l, getMarginal((MSFactor)f).getKeyWithBestValue());
		}
	}
}
