package seas2.maxsum;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import seas2.core.Link;

public abstract class Factor {
	private Set<Factor> inNeighbors = new HashSet<Factor>();
	private Set<Factor> outNeighbors = new HashSet<Factor>();
	private Set<Factor> neighbors = new HashSet<Factor>();
	private Map<Factor, Link> links = new HashMap<Factor,Link>();
	
	private String name;
	
	private  void addInNeighbor(Factor inNeighbor) {
		this.inNeighbors.add(inNeighbor);
		this.neighbors.add(inNeighbor);
	}
	
	private void addOutNeighbor(Factor outNeighbor) {
		this.outNeighbors.add(outNeighbor);
		this.neighbors.add(outNeighbor);
	}
	
	public void addInNeighbor(Factor inNeighbor, Link l) {
		addInNeighbor(inNeighbor);
		links.put(inNeighbor, l);
	}
	
	public void addOutNeighbor(Factor outNeighbor, Link l) {
		addOutNeighbor(outNeighbor);
		links.put(outNeighbor, l);
	}

	public Set<Factor> getInNeighbors() {
		return Collections.unmodifiableSet(inNeighbors);
	}
	
	public Set<Factor> getOutNeighbors() {
		return Collections.unmodifiableSet(outNeighbors);
	}
	
	public Collection<Link> getLinks() {
		return Collections.unmodifiableCollection(links.values());
	}
	
	public Link getLink(Factor f) {
		return links.get(f);
	}
	
	public Set<Factor> getNeighbors() {
		return Collections.unmodifiableSet(neighbors);
	}
	
	public int getNumNeighbors() {
		return neighbors.size();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
