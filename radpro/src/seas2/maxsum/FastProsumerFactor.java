package seas2.maxsum;


/**
 * Factor that represents a prosumer
 *  */
public  class FastProsumerFactor extends ProsumerFactor {
	public FastProsumerFactor(String name, MSValuation offer) {
		super(name, offer);
	}
	
	public MSValuation assessMessage(MSFactor neighbor) {
		MSValuation msg;
		
	    //System.out.println("Offer is:"+offer);
		MSValuation sumOthers = aggregateAllMessagesButThisOne(neighbor);
		//System.out.println("Others aggregation:" + sumOthers);
		//MSValuation negSumOthers = sumOthers.negate();
		//System.out.println("After negate:" + negSumOthers);
		msg = sumOthers.aggregateBeliefs(offer);
		//System.out.println("After summing:" + msg);
		if (getOutNeighbors().contains(neighbor)) {
			msg = msg.negate();
		}
		msg.filter(getLink(neighbor).capacity);
		return msg;
	}
	
	protected MSValuation aggregateAllMessagesButThisOne(MSFactor thisOne) {
		MSValuation beliefSum = new MSValuation();
		beliefSum = aggregateInMessagesButThisOne(beliefSum,thisOne);
		beliefSum = beliefSum.negate();
		beliefSum = aggregateOutMessagesButThisOne(beliefSum,thisOne);
		return beliefSum;
	}
	
	private MSValuation aggregateInMessagesButThisOne(MSValuation beliefSum, MSFactor thisOne) {
		for (Factor f: getInNeighbors()) {
			if (f != thisOne) {
				beliefSum = beliefSum.aggregateBeliefs(getMessage((MSFactor)f));
			}
		}
		return beliefSum;
	}

	private MSValuation aggregateOutMessagesButThisOne(MSValuation beliefSum, MSFactor thisOne) {
		for (Factor f: getOutNeighbors()) {
			if (f != thisOne) {
				beliefSum = beliefSum.aggregateBeliefs(getMessage((MSFactor)f));
			}
		}
		return beliefSum;
	}
}
