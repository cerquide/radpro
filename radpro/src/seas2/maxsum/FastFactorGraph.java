package seas2.maxsum;

import seas2.core.OneWay;
import seas2.core.Participant;
import seas2.core.Problem;

public class FastFactorGraph extends FactorGraph {
	public FastFactorGraph(Problem problem) {
		super(problem);
	}

	protected ProsumerFactor makeParticipantFactor(Participant p) {
		String name = (p instanceof OneWay ? "b" : "p") + p.getId();
		System.out.println("Creating participant " + name);
		MSValuation valuation = new MSValuation(p.getValuation());
		valuation.addNoise(NOISE);
		ProsumerFactor f;
		if (p instanceof OneWay)
			f = new OneWayFactor(name, valuation);
		else
			f = new FastProsumerFactor(name, valuation);
		return f;
	}

}
