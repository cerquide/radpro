package seas2.maxsum;

public class OneWayFactor extends ProsumerFactor {

	public OneWayFactor(String name, MSValuation offer) {
		super(name, offer);
	}

	/* (non-Javadoc)
	 * @see seas2.maxsum.FastProsumerFactor#assessMessage(seas2.maxsum.MSFactor)
	 */
	public MSValuation assessMessage(MSFactor neighbor) {
		MSValuation msg = new MSValuation();
		msg = msg.aggregateBeliefs(this.offer);
		if (getOutNeighbors().contains(neighbor)) {
			msg = msg.negate();
		}
		msg.filter(getLink(neighbor).capacity);
		return msg;
	}
}
