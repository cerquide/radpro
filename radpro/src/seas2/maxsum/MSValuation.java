package seas2.maxsum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import seas2.core.IndexIterator;
import seas2.core.StateValuation;

public class MSValuation extends HashMap<Integer, Double> {

	public final static Random randomGenerator = new Random(1);
	// public static ArrayList<Double> generatedRandomNumbers = new
	// ArrayList<Double>();

	private static final long serialVersionUID = 6728025861850690964L;

	// Creates an initial belief that shows indifference to each of the possible
	// states of the link.
	// This is used for the initial messages
	public MSValuation(int capacity) {
		super();
		for (int i = -capacity; i <= capacity; i++) {
			put(i, 0.0);
		}
	}

	// Creates an empty belief, which is later going to be completed manually
	public MSValuation() {
		super();
		put(0, 0.0);
	}

	// Copy constructor
	public MSValuation(MSValuation b) {
		super(b);
	}

	public MSValuation(StateValuation valuation) {
		super(valuation);
	}

	public static MSValuation bruteForceAggregation(List<MSValuation> vl) {
		List<Set<Integer>> domains = new ArrayList<Set<Integer>>();
		for (MSValuation v : vl) {
			domains.add(v.keySet());
		}
		IndexIterator indexIterator = new IndexIterator(domains);
		List<Integer> index;
		MSValuation aggregate = new MSValuation();
		int k;
		double value;
		int x_i;
		while (indexIterator.hasNext()) {
			index = indexIterator.next();
			// System.out.println(index);
			k = 0;
			value = 0.0;
			for (int i = 0; i < index.size(); i++) {
				x_i = index.get(i);
				k += x_i;
				value += vl.get(i).get(x_i);
			}
			// System.out.println("Considering pair "+k+","+value);
			aggregate.consider(k, value);
		}
		return aggregate;
	}

	// Assesses the merge of two beliefs brute force style.
	public MSValuation bruteForceAggregateBeliefs(MSValuation m2) {
		List<MSValuation> vl = new ArrayList<MSValuation>(2);
		vl.add(this);
		vl.add(m2);
		MSValuation aggregate = MSValuation.bruteForceAggregation(vl);
		return aggregate;
	}

	// Assesses the merge of two beliefs.
	public MSValuation aggregateBeliefs(MSValuation m2) {
		// System.out.println("Aggregating");
		// System.out.println(this);
		// System.out.println(m2);
		MSValuation aggregate = new MSValuation();
		// for (Integer j : m2.keySet()) {
		// aggregate.consider( j, m2.get(j));
		// }
		for (Integer i : this.keySet()) {
			// aggregate.consider( i, this.get(i));
			for (Integer j : m2.keySet()) {
				int k = i + j;
				aggregate.consider(k, this.get(i) + m2.get(j));
			}
		}
		// MSValuation aggregate2 = bruteForceAggregateBeliefs(m2);
		// if (aggregate.distance(aggregate2) > 1e-6) {
		// System.out.println("ERRORRRRRRRRRRRRR");
		// System.out.println(aggregate);
		// System.out.println(aggregate2);
		// }
		return aggregate;
	}

	// Given a belief, adds the corresponding (k,value) pair if the key does not
	// exist or the new value is higher
	public void consider(Integer k, double value) {
		if (!containsKey(k) || (value > get(k))) {
			put(k, value);
		}
	}

	public MSValuation negate() {
		MSValuation negatedMessage = new MSValuation();
		for (Map.Entry<Integer, Double> pair : entrySet()) {
			negatedMessage.put(-pair.getKey(), pair.getValue());
		}
		return negatedMessage;
	}

	/**
	 * Computes the sum of (resp. difference between) two beliefs by adding
	 * (resp. subtracting) values for each common entry. If an entry does not
	 * appear in the two beliefs, it will not appear in the result (worst value
	 * by convention).
	 * 
	 * @param m1
	 *            the belief to add to
	 * @param m2
	 *            the belief to add
	 * @return a belief containing all the common keys and values obtained by
	 *         adding/subtracting values from m1 and m2
	 */
	public MSValuation sumBeliefs(MSValuation m2) {
		MSValuation sum = new MSValuation();
		HashSet<Integer> keys = new HashSet<Integer>(this.keySet());
		keys.addAll(new HashSet<Integer>(m2.keySet()));
		for (Integer i : keys) {
			if (this.containsKey(i) && m2.containsKey(i)) {
				sum.put(i, this.get(i) + m2.get(i));
			}
		}
		return sum;
	}

	// Filters a belief so that it only contains the keys corresponding to the
	// capacity of the link.
	public void filter(int capacity) {
		Iterator<Integer> it = keySet().iterator();
		while (it.hasNext()) {
			int i = it.next();
			if ((i > capacity) || (i < -capacity)) {
				it.remove();
			}
		}
	}

	// Assesses the distance between two beliefs
	public double distance(MSValuation m2) {
		HashSet<Integer> keys = new HashSet<Integer>(this.keySet());
		keys.addAll(new HashSet<Integer>(m2.keySet()));
		double sum = 0.0;
		for (Integer i : keys) {
			if (this.containsKey(i) && m2.containsKey(i)) {
				sum += (this.get(i) - m2.get(i)) * (this.get(i) - m2.get(i));
			} else {
				return Double.MAX_VALUE;
			}
		}
		return sum;
	}

	public int getKeyWithBestValue() {
		double maxValue = -Double.MAX_VALUE;
		int bestKey = -Integer.MAX_VALUE;
		for (Map.Entry<Integer, Double> pair : this.entrySet()) {
			if (pair.getValue() > maxValue) {
				bestKey = pair.getKey();
				maxValue = pair.getValue();
			}
		}
		return bestKey;
	}

	public void addNoise(double percentage) {
		// while (generatedRandomNumbers.contains(noise))
		// noise = randomGenerator.nextDouble();
		// generatedRandomNumbers.add(noise);
		for (Map.Entry<Integer, Double> pair : this.entrySet()) {
			double noise = randomGenerator.nextDouble();
			double newValue = pair.getValue() + ((noise - 0.5) * percentage);
			put(pair.getKey(), newValue);
		}
	}

	public int getLargestKey() {
		int max = -Integer.MAX_VALUE;
		for (int i : keySet()) {
			if (i > max) {
				max = i;
			}
		}
		return max;
	}

	public int getSmallestKey() {
		int min = Integer.MAX_VALUE;
		for (int i : keySet()) {
			if (i < min) {
				min = i;
			}
		}
		return min;
	}

}
