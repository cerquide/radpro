package seas2.core.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import seas2.core.Assignment;
import seas2.core.Link;

public final class AssignmentExporter {

	public static void exportAssignmentToJSON(Assignment assignment, String path) {
		String s = "{ \"assignment\": [";
		int n = 0;
		for (Map.Entry<Link, Integer> e : assignment.entrySet()) {
			s += "{\"variable:\"" + e.getKey().getVariableName() + ", "
					+ "\"value\": " + e.getValue() + "}";
			if (++n < assignment.size())
				s += ",";
		}
		s += "]}";
		try {
			File file = new File(path);
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(s);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void exportAssignmentToCSV(Assignment assignment, String path) {
		String s = "name,value\n";
		int n = 0;
		for (Map.Entry<Link, Integer> e : assignment.entrySet()) {
			s += e.getKey().getVariableName() + "," + e.getValue();
			if (++n < assignment.size())
				s += "\n";
		}
		try {
			File file = new File(path);
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(s);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
