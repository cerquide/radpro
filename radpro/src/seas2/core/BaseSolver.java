package seas2.core;

public abstract class BaseSolver implements Solver {
	public Options timed_solve(Problem problem, Options o) {
		long timeStart = System.currentTimeMillis();
		Options result = solve(problem, o);
		long timeEnd = System.currentTimeMillis();
		result.put(time, timeEnd-timeStart);
		return result;
	}
}
