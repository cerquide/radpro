package seas2.core;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;

@SuppressWarnings("serial")
public class Options extends HashMap<String, Object> {

	public void saveToJSON(String outfile) {
		Gson gson= new Gson();
		String result = gson.toJson(this);
		try {  
			//write converted json data to a file named "CountryGSON.json"  
			FileWriter writer = new FileWriter(outfile);  
			writer.write(result);  
			writer.close();  
		} catch (IOException e) {  
			e.printStackTrace();  
		}  
	}
}
