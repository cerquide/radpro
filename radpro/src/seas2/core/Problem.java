package seas2.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem implements Cloneable {
	private Map<Long, Participant> participants = new HashMap<Long, Participant>();
	private List<Link> links = new ArrayList<Link>();

	public void addParticipant(Participant p) {
		participants.put(p.getId(), p);
	}

	public Participant getParticipantById(long id) {
		return participants.get(id);
	}

	public void addLink(long source, long target, int capacity) {
		Participant pSource = getParticipantById(source);
		Participant pTarget = getParticipantById(target);
		Link l = new Link(pSource, pTarget, capacity);
		links.add(l);
		pTarget.addOutLink(l);
		pSource.addInLink(l);
	}

	public Collection<Participant> getParticipants() {
		return Collections.unmodifiableCollection(participants.values());
	}

	public Collection<Link> getLinks() {
		return Collections.unmodifiableCollection(links);
	}

	public Link findLink(String varName) {
		for (Link l : links) {
			if (l.getVariableName().equals(varName)) {
				return l;
			}
		}
		return null;
	}

	public String toString() {
		StringBuffer s = new StringBuffer();
		for (Participant p : participants.values()) {
			s.append(p.toString() + "\n");
		}
		for (Link l : links) {
			s.append(l.toString() + "\n");
		}
		return s.toString();
	}

	public double eval(Assignment assignment) {
		double eval = 0;
		for (Participant p : participants.values()) {
			eval += p.eval(assignment);
		}
		return eval;
	}

	public double getAverageNeighbourhoodSize() {
		double size = 0;
		for (Participant p : participants.values()) {
			size += p.getInLinks().size() + p.getOutLinks().size();
		}
		return size / participants.values().size();
	}
}
