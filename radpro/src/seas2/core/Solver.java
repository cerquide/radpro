package seas2.core;

public interface Solver {
	public static final String solution = new String("solution");
	public static final String eval = new String("eval");
	public static final String time = new String("time");

	public Options solve(Problem problem, Options m);
	public Options timed_solve(Problem problem, Options o);
}
