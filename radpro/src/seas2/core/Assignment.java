package seas2.core;

import java.util.HashMap;
import java.util.Map;


@SuppressWarnings("serial")
public class Assignment extends HashMap<Link,Integer> {
	public Assignment() {
		super();
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer("[");
		for (Map.Entry<Link,Integer> e:entrySet()) {
			s.append(e.getKey().getVariableName());
			s.append("=");
			s.append(e.getValue().toString());
			s.append(";");
		}
		s.append("]");
		return s.toString();
	}

	public Assignment extend(Assignment a2) {
		Assignment a = new Assignment();
		a.putAll(this);
		a.putAll(a2);
		return a;
	}
}
