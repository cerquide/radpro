package seas2.core;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Participant {
	private long id;
	private StateValuation valuation;
	private Set<Link> outLinks = new HashSet<Link>();
	private Set<Link> inLinks = new HashSet<Link>();

	public Participant(long id, StateValuation v) {
		super();
		this.id = id;
		valuation = v;
	}

	public String toString() {
		return "{id: " + id + ", valuation:" + valuation + "}";
	}

	public long getId() {
		return id;
	}

	public StateValuation getValuation() {
		return valuation;
	}

	public void addOutLink(Link l) {
		outLinks.add(l);
	}

	public void addInLink(Link l) {
		inLinks.add(l);
	}

	public Set<Link> getOutLinks() {
		return Collections.unmodifiableSet(outLinks);
	}

	public Set<Link> getInLinks() {
		return Collections.unmodifiableSet(inLinks);
	}

	public double eval(Assignment a) {
		int myState = 0;
		//System.out.println("PARTICIPANT : " + this);
		//System.out.println("\t - Assignment : " + a);
		//System.out.println("\t - InLinks : " + getInLinks());
		//System.out.println("\t - OutLinks : " + getOutLinks());

		for (Link l : getInLinks()) {
			myState -= a.get(l);
		}
		for (Link l : getOutLinks()) {
			myState += a.get(l);
		}
		//System.out.println("\t - State : " + myState);
		//System.out.println("\t - Value : " + valuation.get(myState));
		double myValue = (valuation.get(myState) == null ? 0 : valuation
				.get(myState));
		// System.out.println("Participant "+ id +" is set in state "+myState+
		// " with value "+myValue);
		// TODO : FIX THAT UGLY FIX !!!
		return myValue;// (Double.isNaN(myValue) ? 0.0 : myValue);
	}
}
