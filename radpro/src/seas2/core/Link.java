package seas2.core;

public class Link {
	public int capacity;
	public Participant source;
	public Participant dest;
	
	public Link(Participant source, Participant dest, int capacity) {
		this.source = source;
		this.dest = dest;
		this.capacity = capacity;
	}

	public String getVariableName() {
		return "y_" + source.getId() + "_"+ dest.getId();
	}
	
	public String toString() {
		return "y_" + source.getId() + "_"+ dest.getId();
		//return "{source:" + source + ", dest: " + dest + ", capacity:" + capacity + "}";
	}
}
