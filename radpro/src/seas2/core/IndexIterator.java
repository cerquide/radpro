package seas2.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class IndexIterator implements Iterator<List<Integer>> {
	private ArrayList<Iterator<Integer>> its;
	private ArrayList<Integer> current;
	private List<Set<Integer>> domains;
	private boolean hasMore;
	private boolean started;
	public IndexIterator(List<Set<Integer>> domains) {
		this.domains = domains;
		its = new ArrayList<Iterator<Integer>>(domains.size());
		current = new ArrayList<Integer>(domains.size());
		for (int i = 0; i < domains.size() ; i++) {
			its.add(i,Collections.<Integer>emptyList().iterator());
			current.add(i,0);
		}
		hasMore = true;
		started = false;
	}
	
	@Override
	public boolean hasNext() {
		return hasMore;
	}
	
	@Override
	public List<Integer> next() {
		int firstWithNext = 0;
		while ((firstWithNext < its.size()) && !its.get(firstWithNext).hasNext() ) {
			firstWithNext++;
		} 
		//System.out.println(firstWithNext);
		for (int i = 0 ; i < firstWithNext ; i++) {
			its.set(i,domains.get(i).iterator());
			current.set(i,its.get(i).next());
		}
		//System.out.println(its.size());
		if (firstWithNext < its.size()) {
			current.set(firstWithNext,its.get(firstWithNext).next());
			if (its.size()==1) {
				hasMore = its.get(firstWithNext).hasNext() || (firstWithNext != (its.size()-1));
			}
		} else {
			if (started || its.size()==0) {
				hasMore = false;
			} else {
				started = true;
			}
		}
		return current;
	}
	
	@Override
	public void remove() {}
	
}
