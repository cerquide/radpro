package seas2.nonlinear;

import seas2.core.MaxUnit;
import seas2.core.MinUnit;
import seas2.core.Participant;
import seas2.core.UnitCost;

public class NonLinearParticipant extends Participant implements UnitCost, MaxUnit, MinUnit {
	private int min;
	private int max;
	private double unitCost;
	public NonLinearParticipant(long id, int min, int max, double unitCost, double gamma) {
		super(id,new NonLinearValuation(min,max,unitCost, gamma));
		this.min = min;
		this.max = max;
		this.unitCost = unitCost;
	}
	
	public int getMax() {
		return max;
	}
	
	public int getMin() {
		return min;
	}
	
	public double getUnitCost() {
		return unitCost;
	}
}
