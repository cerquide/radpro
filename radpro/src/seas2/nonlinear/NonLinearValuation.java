package seas2.nonlinear;

import seas2.core.StateValuation;


@SuppressWarnings("serial")
public class NonLinearValuation extends StateValuation {
	public NonLinearValuation(int min, int max, double unitCost, double gamma) {
		super();
		put(0,0.0);
		if (unitCost < 0.0) {
			int oldMin = min; 
			min = Math.min(-min, -max);
			max = Math.max(-oldMin, -max);
			unitCost = -unitCost;
		}
		for (int i = min; i <= max ; i++) {
			put(i, i * unitCost * Math.pow(gamma, Math.abs(i)-1));
		}
	}
}
