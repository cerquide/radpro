package seas2.linear;

import seas2.core.StateValuation;


@SuppressWarnings("serial")
public class LinearValuation extends StateValuation {
	public LinearValuation(int min, int max, double unitCost) {
		super();
		put(0,0.0);
		if (unitCost < 0.0) {
			int oldMin = min; 
			min = Math.min(-min, -max);
			max = Math.max(-oldMin, -max);
			unitCost = -unitCost;
		}
		for (int i = min; i <= max ; i++) {
			put(i, i * unitCost);
		}
	}
}
