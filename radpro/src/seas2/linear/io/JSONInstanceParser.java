package seas2.linear.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import seas2.core.Problem;
import seas2.linear.LinearParticipant;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Simple class that parse a JSON file containing a SEAS prosumer problem into a
 * List<Participant> object.
 * 
 * @author picard
 * 
 */
public class JSONInstanceParser {

	/**
	 * Return a list of participants from a JSON file, output of the python
	 * graph generator.
	 * 
	 * @param file
	 *            the JSON file to parse
	 * @return a List of Participants
	 */
	public static Problem parse(File file) {
		Problem problem  = new Problem();
		try {
			JsonParser parser = new JsonParser();
			Object obj = parser.parse(new FileReader(file));
			JsonObject jsonObject = (JsonObject) obj;
			JsonArray nodes = (JsonArray) jsonObject.get("nodes");
			JsonArray edges = (JsonArray) jsonObject.get("links");

			Iterator<JsonElement> iterator = nodes.iterator();
			
			while (iterator.hasNext()) {
				JsonObject node = iterator.next().getAsJsonObject();
				long id = node.get("id").getAsLong();
				int min = node.get("min").getAsInt();
				int max = node.get("max").getAsInt();
				double cost = node.get("cost").getAsDouble();
				LinearParticipant p = new LinearParticipant(id, min,max,cost);
				//participants.add(p);
				problem.addParticipant(p);
				//System.out.println(p);
			}

			iterator = edges.iterator();
			while (iterator.hasNext()) {
				JsonObject edge = iterator.next().getAsJsonObject();
				long source = edge.get("source").getAsLong();
				long target = edge.get("target").getAsLong();
				LinearParticipant pSeller = (LinearParticipant) problem.getParticipantById(source);
				LinearParticipant pBuyer = (LinearParticipant) problem.getParticipantById(target);
				int capacity = Math.min(pSeller.getMax(), pBuyer.getMax());
				problem.addLink(source, target, capacity);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return problem;

	}

	/**
	 * How to use the simple parser...
	 * 
	 * @param args
	 *            the first argument MUST be the JSON file name
	 */
	public static void main(String[] args) {
		Problem problem = parse(new File(args[0]));
		System.out.println(problem);
	}
}
