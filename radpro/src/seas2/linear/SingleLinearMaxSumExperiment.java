package seas2.linear;


import java.io.File;
import java.io.IOException;

import seas2.core.Assignment;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.linear.io.JSONInstanceParser;
import seas2.maxsum.MaxSumSolver;

public class SingleLinearMaxSumExperiment {

	public static void main(String[] args) throws IOException {
		runMaxSum(args[0]);
	}
	
	public static int runMaxSum(String infile) throws IOException {
		Problem problem = JSONInstanceParser.parse(new File(infile));
		//System.out.println("PROBLEM");
		//System.out.println(problem);
		MaxSumSolver msf = new MaxSumSolver();
		Options o = new Options();
		o.put(MaxSumSolver.maxSteps, problem.getParticipants().size() * 1000);
		o.put(MaxSumSolver.minError, 1e-6);
		long timeStart = System.currentTimeMillis();
		Options results = msf.solve(problem, o);
		long timeEnd = System.currentTimeMillis();
		Assignment sol = (Assignment) results.get(Solver.solution);
		int steps = (Integer) results.get(MaxSumSolver.numSteps);
		int maxSteps = (Integer) o.get(MaxSumSolver.maxSteps);
		System.out.println(""+ steps + "," + maxSteps + "," + problem.eval(sol) + "," 
				+ (timeEnd - timeStart));
		//System.out.println("Steps:"+ steps + "," + maxSteps + ",Solution value:" + problem.eval(sol) + ","
		//		+ (timeEnd - timeStart) + " Solution:"+ sol);
		return 0;
	}

}
