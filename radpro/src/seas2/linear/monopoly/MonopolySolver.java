package seas2.linear.monopoly;

// import java.util.HashMap;

import seas2.core.Assignment;
import seas2.core.BaseSolver;
import seas2.core.Link;
import seas2.core.Options;
import seas2.core.Participant;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.linear.LinearParticipant;
import seas2.linear.monopoly.utils.ProblemTools;

public class MonopolySolver  extends BaseSolver implements Solver {

	public static final String suffix = new String("Monopoly");

	public static final String buyerPrice = new String("buyerPrice");
	public static final String sellerPrice = new String("sellerPrice");
	public static final String sells = new String("sells");
	public static final String buys = new String("buys");
	public static final String monopolyBuyerPricePos = new String(
			"monopolyBuyerPricePos");
	public static final String monopolySellerPricePos = new String(
			"monopolySellerPricePos");
	public static final String nbBigParticipantsUnits = new String(
			"nbBigParticipantsUnits" + suffix);
	public static final String bigParticipantsProfit = new String(
			"bigParticipantsProfit" + suffix);
	public static final String nbTotalUnits = new String("nbTotalUnits"
			+ suffix);
	public static final String solution = new String("solution" + suffix);
	public static final String eval = new String("eval"+suffix);

	@Override
	public Options solve(Problem problem, Options m) {
		return solveMonopoly(ProblemTools.addBigOnes(problem, m), m);
	}

	private Options solveMonopoly(Problem problem, Options prices) {
		Options result = new Options();
		result.putAll(prices);
		double monopolyBuyerPrice = (Double) (result.get("buyerPrice"));
		double monopolySellerPrice = (Double) (result.get("sellerPrice"));
		Assignment assignment = new Assignment();
		for (Link l : problem.getLinks()) {
			Participant source = l.source;
			Participant dest = l.dest;
			assignment.put(l, 0);
			LinearParticipant small = null;
			int sign = 1;
			if (source instanceof BigParticipant)
				small = (LinearParticipant) dest;
			else if (dest instanceof BigParticipant) {
				small = (LinearParticipant) source;
				sign = -1;
			}
			if (small != null) {
				int units = small.getMax();
				double cost = small.getUnitCost();
				if (((cost > 0) && (cost >= monopolySellerPrice))
						|| ((cost < 0) && (cost >= monopolyBuyerPrice)))
					assignment.put(l, sign * units);
			}
		}
		result.put(Solver.solution, assignment);
		result.put(MonopolySolver.solution, assignment);
		return result;
	}
}
