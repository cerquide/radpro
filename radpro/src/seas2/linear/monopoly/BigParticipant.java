package seas2.linear.monopoly;

import seas2.core.OneWay;
import seas2.linear.LinearParticipant;

public class BigParticipant extends LinearParticipant implements OneWay {

	public BigParticipant(long id, int min, int max, double unitCost) {
		super(id, min, max, unitCost);
	}

}
