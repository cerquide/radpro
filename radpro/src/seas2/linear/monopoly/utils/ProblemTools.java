package seas2.linear.monopoly.utils;

import java.util.ArrayList;
import java.util.List;

import seas2.core.Assignment;
import seas2.core.Link;
import seas2.core.Options;
import seas2.core.Participant;
import seas2.core.Problem;
import seas2.linear.LinearParticipant;
import seas2.linear.monopoly.BigParticipant;
import seas2.linear.monopoly.MonopolySolver;
import seas2.core.UnitCost;
import seas2.core.MaxUnit;

public class ProblemTools {

	/**
	 * Computes the production and consumption prices given some positioning
	 * ratio, with respect to all the costs provided by the prosumers.
	 * 
	 * @param problem
	 *            the problem to analyze
	 * @param sellerPricePositioning
	 *            the position of the big producer price (a percentage between
	 *            the minimum production cost and the maximum production cost)
	 * @param buyerPricePositioning
	 *            the position of the big consumer price (a percentage between
	 *            the minimum consumption cost and the maximum consumption cost)
	 * @return
	 */
	public static Options findPrice(Problem problem,
			double sellerPricePositioning, double buyerPricePositioning) {
		Options result = new Options();
		double maxBuyer = -Double.MAX_VALUE;
		double minBuyer = Double.MAX_VALUE;
		double maxSeller = -Double.MAX_VALUE;
		double minSeller = Double.MAX_VALUE;
		for (Participant p : problem.getParticipants()) {
			double cost = ((UnitCost) p).getUnitCost();
			if (cost > 0.0) {
				// buyer participant:
				if (cost > maxBuyer)
					maxBuyer = cost;
				if (cost < minBuyer)
					minBuyer = cost;
			} else {
				// seller participant:
				if (cost > maxSeller)
					maxSeller = cost;
				if (cost < minSeller)
					minSeller = cost;
			}
		}

		if (maxBuyer == -Double.MAX_VALUE)
			maxBuyer = 0.0;
		if (maxSeller == -Double.MAX_VALUE)
			maxSeller = 0.0;
		if (minBuyer == Double.MAX_VALUE)
			minBuyer = 0.0;
		if (minSeller == Double.MAX_VALUE)
			minSeller = 0.0;
		result.put("buyerPrice", (maxBuyer - minBuyer) * sellerPricePositioning
				+ minBuyer);
		result.put("sellerPrice", (minSeller - maxSeller)
				* buyerPricePositioning + maxSeller);
		return result;
	}

	/**
	 * Transform a given problem by adding two new Participant : a big producer
	 * connected to all the consumers, and a big consumer connected to all the
	 * producers
	 * 
	 * @param problem
	 *            the problem to transform
	 * @param m
	 *            some options specifying the big ones' costs
	 *            (MonopolySolver.sellerPrice and MonopolySolver.buyerPrice)
	 * @return the extended problem
	 */
	public static Problem addBigOnesCPLEX(Problem problem, Options m) {
		double bigProducerCost = -1.0;
		double bigConsumerCost = 1.0;
		if (m.containsKey(MonopolySolver.sellerPrice))
			bigProducerCost = (Double) m.get(MonopolySolver.sellerPrice);
		if (m.containsKey(MonopolySolver.buyerPrice))
			bigConsumerCost = (Double) m.get(MonopolySolver.buyerPrice);
		int[] maxUnits = getMaxUnits(problem);
		int nbParticipants = problem.getParticipants().size();
		// create the big ones:
		Participant bigProducer = new BigParticipant(nbParticipants, 0,
				maxUnits[0], bigProducerCost);
		Participant bigConsumer = new BigParticipant(nbParticipants + 1,
				0, maxUnits[1], bigConsumerCost);
		// add links from the big ones to every participant:
		problem.addParticipant(bigProducer);
		problem.addParticipant(bigConsumer);
		for (Participant p : problem.getParticipants()) {
			if (p.getId() < nbParticipants)
				if (((UnitCost) p).getUnitCost() >= 0)
					problem.addLink(p.getId(), bigProducer.getId(),
							((MaxUnit) p).getMax());
				else
					problem.addLink(p.getId(), bigConsumer.getId(),
							((MaxUnit) p).getMax());
		}
		return problem;
	}

	/**
	 * Transform a given problem by adding two new Participant : a big producer
	 * connected to all the consumers, and a big consumer connected to all the
	 * producers
	 * 
	 * @param problem
	 *            the problem to transform
	 * @param m
	 *            some options specifying the big ones' costs
	 *            (MonopolySolver.sellerPrice and MonopolySolver.buyerPrice)
	 * @return the extended problem
	 */
	public static Problem addBigOnes(Problem problem, Options m) {
		double bigProducerCost = -1.0;
		double bigConsumerCost = 1.0;
		if (m.containsKey(MonopolySolver.sellerPrice))
			bigProducerCost = (Double) m.get(MonopolySolver.sellerPrice);
		if (m.containsKey(MonopolySolver.buyerPrice))
			bigConsumerCost = (Double) m.get(MonopolySolver.buyerPrice);
		int[] maxUnits = getMaxUnits(problem);
		int nbParticipants = problem.getParticipants().size();
		int lastParticipantId = nbParticipants - 1;
		List<Participant> l = new ArrayList<Participant>(
				problem.getParticipants());
		for (Participant p : l) {
			if (((LinearParticipant) p).getUnitCost() >= 0) {
				// create the big producer:
				LinearParticipant bigProducer = new BigParticipant(
						++lastParticipantId, 0, maxUnits[0], bigProducerCost);
				// add links from the big producer to every consumer:
				problem.addParticipant(bigProducer);
				problem.addLink(bigProducer.getId(), p.getId(),
						((LinearParticipant) p).getMax());
			} else {
				// create the big consumer:
				LinearParticipant bigConsumer = new BigParticipant(
						++lastParticipantId, 0, maxUnits[1], bigConsumerCost);
				// add links from the big consumer to every producer:
				problem.addParticipant(bigConsumer);
				problem.addLink(bigConsumer.getId(), p.getId(),
						((LinearParticipant) p).getMax());
			}
		}
		return problem;
	}

	/**
	 * Computes the theoretical max numbers of produced (resp. consumed) units
	 * for a given problem
	 * 
	 * @param problem
	 *            the problem to analyze
	 * @return an array with two values (the maximum number of consumed units
	 *         and the maximum number of produced units)
	 */
	public static int[] getMaxUnits(Problem problem) {
		int[] result = new int[2];
		result[0] = 0;
		result[1] = 0;
		for (Participant p : problem.getParticipants()) {
			if (((LinearParticipant) p).getUnitCost() >= 0)
				// units consumed
				result[0] = result[0] + ((LinearParticipant) p).getMax();
			else
				// units produced
				result[1] = result[1] + ((LinearParticipant) p).getMax();
		}
		return result;
	}

	/**
	 * @param a
	 *            an assignment to count
	 * @return the number of units traded by BigParticipants
	 */
	public static int getNbBigParticipantsUnits(Assignment a) {
		int total = 0;
		for (Link l : a.keySet()) {
			if ((l.source instanceof BigParticipant)
					|| (l.dest instanceof BigParticipant))
				total += a.get(l);
		}
		return total;
	}

	/**
	 * @param a
	 *            an assignment to count
	 * @return the total number of units exchanged in the assignment
	 */
	public static int getNbTotalUnits(Assignment a) {
		int total = 0;
		for (Link l : a.keySet())
			total += a.get(l);
		return total;
	}

	/**
	 * @param a
	 *            an assignment to count
	 * @return the profit of the BigParticipants
	 */
	public static double getBigOnesProfit(Assignment a) {
		double profit = 0;
		for (Link l : a.keySet()) {
			BigParticipant big = null;
			if (l.source instanceof BigParticipant)
				big = (BigParticipant) (l.source);
			if (l.dest instanceof BigParticipant)
				big = (BigParticipant) (l.dest);
			if (big != null) {
				profit += a.get(l) * big.getUnitCost();
			}
		}
		return profit;
	}
}
