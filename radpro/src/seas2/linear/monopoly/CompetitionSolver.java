package seas2.linear.monopoly;

import seas2.actiongdl.FastActionGDLSolver;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.linear.monopoly.utils.ProblemTools;
import seas2.maxsum.FastMaxSumSolver;

public class CompetitionSolver extends FastActionGDLSolver {

	public static final String suffix = new String("Competition");
	
	public static final String bigParticipantsProfit = new String("bigParticipantsProfit"+suffix);
	public static final String nbBigParticipantsUnits = new String("nbBigParticipantsUnits"+suffix);
	public static final String nbTotalUnits = new String("nbTotalUnits"+suffix);
	public static final String minError = new String("minError"+suffix);
	public static final String maxSteps = new String("maxSteps"+suffix);
	public static final String numSteps = new String("numSteps"+suffix);
	public static final String numMessages = new String("numMessages"+suffix);
	public static final String numTransactions = new String("numTransactions"+suffix);
	public static final String solution = new String("solution"+suffix);
	public static final String eval = new String("eval"+suffix);
	public static final String time = new String("time"+suffix);

	@Override
	public Options solve(Problem problem, Options m) {
		Problem p = ProblemTools.addBigOnes(problem, m);
		System.out.println(p);
		Options result = super.solve(p, m);
		result.put(CompetitionSolver.solution, result.get(Solver.solution));
		result.put(CompetitionSolver.numSteps, result.get(FastMaxSumSolver.numSteps));
		result.put(CompetitionSolver.numMessages, result.get(FastMaxSumSolver.numMessages));
		result.put(CompetitionSolver.numTransactions, result.get(FastMaxSumSolver.numTransactions));
		return result;
	}
}
