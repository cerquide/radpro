package seas2.linear;

import seas2.core.MaxUnit;
import seas2.core.MinUnit;
import seas2.core.Participant;
import seas2.core.UnitCost;

public class LinearParticipant extends Participant implements UnitCost, MaxUnit, MinUnit {
	private int min;
	private int max;
	private double unitCost;
	public LinearParticipant(long id, int min, int max, double unitCost) {
		super(id,new LinearValuation(min,max,unitCost));
		this.min = min;
		this.max = max;
		this.unitCost = unitCost;
	}
	
	public int getMax() {
		return max;
	}
	
	public int getMin() {
		return min;
	}
	
	public double getUnitCost() {
		return unitCost;
	}
}
