package seas2.actiongdl;

import seas2.core.OneWay;
import seas2.core.Participant;
import seas2.core.Problem;

public class FastFactorTree extends FactorTree {
	
	public FastFactorTree(Problem problem) {
		super(problem);
	}

	protected AGDLProsumerFactor makeParticipantFactor(Participant p) {
		String name = (p instanceof OneWay ? "b" : "p") + p.getId();
		//System.out.println("Creating participant " + name);
		//BookkeepedValuation valuation = new BookkeepedValuation(p.getValuation());
		AGDLProsumerFactor f;
		//if (p instanceof OneWay)
		//	f = new OneWayFactor(name, valuation);
		//else
		f = new FastAGDLProsumerFactor(name, p.getValuation());
		return f;
	}

}

