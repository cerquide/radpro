package seas2.actiongdl;

import java.util.HashMap;
import java.util.Map;

import seas2.core.Assignment;
import seas2.core.Link;
import seas2.core.OneWay;
import seas2.core.Participant;
import seas2.core.Problem;
import seas2.maxsum.Factor;

public class FactorTree {
	private Map<Long, AGDLProsumerFactor> prosumerFactors = new HashMap<Long, AGDLProsumerFactor>();
	private int numMessages = 0;
	private int numTransactions = 0;
	private AGDLProsumerFactor root;

	public FactorTree(Problem problem) {
		init(problem);
	}

	protected AGDLProsumerFactor makeParticipantFactor(Participant p) {
		String name = (p instanceof OneWay ? "b" : "p") + p.getId();
		//System.out.println("Creating participant " + name);
		//BookkeepedValuation valuation = new BookkeepedValuation(p.getValuation());
		AGDLProsumerFactor f;
		//if (p instanceof OneWay)
		//	f = new OneWayFactor(name, valuation);
		//else
		f = new AGDLProsumerFactor(name, p.getValuation());
		return f;
	}

	protected void init(Problem problem) {
		for (Participant p : problem.getParticipants()) {
			// Create the participant factors
			AGDLProsumerFactor f = makeParticipantFactor(p);
			prosumerFactors.put(p.getId(), f);
		}

		for (Link l : problem.getLinks()) {
			long sourceId = l.source.getId();
			long destId = l.dest.getId();
			makeNeighbors(prosumerFactors.get(sourceId),
					prosumerFactors.get(destId), l);
		}
	}

	private void makeNeighbors(AGDLProsumerFactor src, AGDLProsumerFactor dst, Link l) {
		src.addOutNeighbor(dst, l);
		dst.addInNeighbor(src, l);
	}

	public Assignment solve(long rootId) {
		root = prosumerFactors.get(rootId);
		root.collect(root);
		Assignment a = new Assignment();
		return root.distribute(root,a);
		//return val.getEquilibriumAssignment();
	}

	public int getNumMessages() {
		numMessages = 0;
		for (AGDLProsumerFactor f : this.prosumerFactors.values())
			numMessages += f.numMessages;
		return numMessages;
	}

	public int getNumTransactions() {
		return numTransactions;
	}

	public Factor getFactor(long id) {
		return prosumerFactors.get(id);
	}

//	private AGDLProsumerFactor getRoot() {
//		return root;
//	}
//
//	private void setRoot(AGDLProsumerFactor root) {
//		this.root = root;
//	}
}

