package seas2.actiongdl;

import seas2.core.Assignment;
import seas2.core.BaseSolver;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;

public class ActionGDLSolver extends BaseSolver implements Solver  {
	
	public static final String suffix = "";//new String("ActionGDL");

	public static final String minError = new String("minError" + suffix);
	public static final String maxSteps = new String("maxSteps" + suffix);
	public static final String numSteps = new String("numSteps" + suffix);
	public static final String numMessages = new String("numMessages" + suffix);
	public static final String numTransactions = new String("numTransactions"
			+ suffix);
	public static final String solution = new String("solution" + suffix);
	public static final String eval = new String("eval" + suffix);
	public static final String time = new String("time" + suffix);
	
	public static final String root = new String("root");
	public static final long DEFAULT_ROOT = 0L;
	
	@Override
	public Options solve(Problem problem, Options m) {
		//System.out.println(problem);
		long rootId = DEFAULT_ROOT;
		if (m.containsKey(root))
			rootId = (Long) m.get(root);
		FactorTree ft = new FactorTree(problem);
		Assignment a = ft.solve(rootId);
		//System.out.println("Solution is " +a);
		Options results = new Options();
		results.put(Solver.solution, a);
		results.put(ActionGDLSolver.solution, a);
		results.put(ActionGDLSolver.numMessages, ft.getNumMessages());
		results.put(ActionGDLSolver.numTransactions, ft.getNumTransactions());
		return results;
	}
}