package seas2.actiongdl;

import seas2.core.StateValuation;
import seas2.maxsum.Factor;

public class FastAGDLProsumerFactor extends AGDLProsumerFactor {
	
	public FastAGDLProsumerFactor(String name, StateValuation valuation) {
		super(name,valuation);
	}
	
	@Override
	public BookkeepedValuation collect(AGDLProsumerFactor parent) {
		// Initialize the valuation
		//System.out.println("Starting collect on:" + getName());
		parent.numMessages++;
		collectedValuation = new BookkeepedValuation(valuation);
		//System.out.println("Initial valuation created : "+collectedValuation);
		// Aggregate valuations from descendants
		for (Factor f: getInNeighbors()) {
			FastAGDLProsumerFactor af = (FastAGDLProsumerFactor) f;
			if (af != parent) {
				//System.out.println("Calling collect on in neighbor "+af.getName());
				BookkeepedValuation val = af.collect(this);
				//System.out.println("Aggregating the result of collect on "+af.getName() + " whose value is "+ val);
				aggregateValuationToCollect(val.negate());
			}
		}
		for (Factor f: getOutNeighbors()) {
			FastAGDLProsumerFactor af = (FastAGDLProsumerFactor) f;
			if (af != parent) {
				//System.out.println("Calling collect on out neighbor "+af.getName());
				BookkeepedValuation val = af.collect(this);
				//System.out.println("Aggregating the result of collect on "+af.getName() + " whose value is "+ val);
				aggregateValuationToCollect(val);
			}
		}
		int capacity = 0;
		if (this != parent) {
			if (getOutNeighbors().contains(parent)) {
				collectedValuation = collectedValuation.negate();
			}
			capacity = getLink(parent).capacity;
		} 
		collectedValuation.filter(capacity);

		//System.out.println("The collectedValuation on "+ getName() + " is "+ collectedValuation);
		if (this!= parent) {
			BookkeepedValuation msg = collectedValuation.recode(getLink(parent));
			//System.out.println("The message from "+ getName() + " to its parent is "+ msg);
			return msg;
		} else {
			return collectedValuation;
		}
	}

	private void aggregateValuationToCollect(BookkeepedValuation val) {
		collectedValuation = collectedValuation.aggregateBeliefs(val);
	}
}
