package seas2.actiongdl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import seas2.core.Assignment;
import seas2.core.Link;
import seas2.core.StateValuation;

public class BookkeepedValuation {

	public final static Random randomGenerator = new Random(1);

	private HashMap<Integer, Double> values;
	private HashMap<Integer, Assignment> argmax;

	public BookkeepedValuation(){
		values = new HashMap<Integer, Double>();
		argmax = new HashMap<Integer, Assignment>();
	}

	public BookkeepedValuation(BookkeepedValuation v){
		values = new HashMap<Integer, Double>(v.values);
		argmax = new HashMap<Integer, Assignment>(v.argmax);
	}

	public BookkeepedValuation(StateValuation valuation) {
		values = new HashMap<Integer, Double>(valuation);
		argmax = new HashMap<Integer, Assignment>();
		for (Map.Entry<Integer,Double> entry : values.entrySet()) {
			Assignment a = new Assignment();
			argmax.put(entry.getKey(), a);
		}
	}

	public BookkeepedValuation(Link l, StateValuation valuation) {
		values = new HashMap<Integer, Double>(valuation);
		argmax = new HashMap<Integer, Assignment>();
		for (Map.Entry<Integer,Double> entry : values.entrySet()) {
			Assignment a = new Assignment();
			a.put(l, entry.getKey());
			argmax.put(entry.getKey(),a );
		}
	}

	// Assesses the merge of two beliefs.
	public BookkeepedValuation aggregateBeliefs(BookkeepedValuation m2) {
		//System.out.println("Aggregating");
		//System.out.println(this);
		//System.out.println(m2);
		BookkeepedValuation aggregate = new BookkeepedValuation();
		for (Integer i : values.keySet()) {
			for (Integer j : m2.values.keySet()) {
				int k = i + j;
				aggregate.consider( k, this, i, m2, j, values.get(i) + m2.values.get(j));
			}
		}
		return aggregate;
	}

	// Given a belief, adds the corresponding (k,value) pair if the key does not exist or the new value is higher
	public void consider( Integer k, Assignment a, double value) {
		if (!values.containsKey(k) || (value > values.get(k))) {
			values.put(k, value);
			argmax.put(k, a);
		}
	}
	// Given a belief, adds the corresponding (k,value) pair if the key does not exist or the new value is higher
	public void consider( Integer k, BookkeepedValuation m1, int i, BookkeepedValuation m2, int j, double value) {
		if (!values.containsKey(k) || (value > values.get(k))) {
			values.put(k, value);
			//System.out.println("i=" + i);
			Assignment a1 = m1.argmax.get(i);
			//System.out.println("a1=" + a1);
			Assignment a2 = m2.argmax.get(j);
			Assignment fusion = a1.extend(a2);
			argmax.put(k, fusion);
		}
	}

	public BookkeepedValuation negate() {
		BookkeepedValuation negatedMessage = new BookkeepedValuation();
		for (int k : values.keySet()) {
			negatedMessage.values.put(-k, values.get(k));
			negatedMessage.argmax.put(-k, argmax.get(k));
		}
		return negatedMessage;
	}

	// Filters a belief so that it only contains the keys corresponding to the capacity of the link.
	public void filter(int capacity) {
		filter(values.keySet().iterator(), capacity);
		filter(argmax.keySet().iterator(), capacity);
	}

	private void filter(Iterator<Integer> it, int capacity) {
		while (it.hasNext()) {
			int i  = it.next();
			if ((i > capacity) || (i < -capacity)) {
				it.remove();
			}
		}
	}

	public int getKeyWithBestValue() {
		double maxValue = - Double.MAX_VALUE;
		int bestKey = -Integer.MAX_VALUE;
		for (Map.Entry<Integer,Double> pair : values.entrySet()) {
			if (pair.getValue() > maxValue) {
				bestKey = pair.getKey();
				maxValue = pair.getValue();
			}
		}
		return bestKey;
	}

	public Assignment getAssignmentWithBestValue() {
		double maxValue = - Double.MAX_VALUE;
		Assignment bestKey = null;
		for (Map.Entry<Integer,Double> pair : values.entrySet()) {
			if (pair.getValue() > maxValue) {
				bestKey = argmax.get(pair.getKey());
				maxValue = pair.getValue();
			}
		}
		return bestKey;
	}

	public Double getValue(int i) {
		return values.get(i);
	}

	public Assignment getAssignmentWithValue(int i) {
		return argmax.get(i);
	}

	public Assignment getEquilibriumAssignment() {
		return argmax.get(0);
	}

	public String toString() {
		return "[values:" + values.toString() + ", argmax:" + argmax.toString() +"]";
	}

	public BookkeepedValuation recode(Link link) {
		BookkeepedValuation recoded = new BookkeepedValuation();
		recoded.values.putAll(values);
		for (Map.Entry<Integer,Double> entry : values.entrySet()) {
			Assignment a = new Assignment();
			a.put(link, entry.getKey());
			recoded.argmax.put(entry.getKey(), a);
		}
		return recoded;
	}

	public Set<Integer> getKeys() {
		return values.keySet();
	}
}

