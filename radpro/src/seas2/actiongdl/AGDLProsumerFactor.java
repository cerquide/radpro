package seas2.actiongdl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import seas2.core.Assignment;
import seas2.core.IndexIterator;
import seas2.core.Link;
import seas2.core.StateValuation;
import seas2.maxsum.Factor;

public class AGDLProsumerFactor extends Factor {
	StateValuation valuation;
	BookkeepedValuation collectedValuation;
	public int numMessages = 0;

	public AGDLProsumerFactor(String name, StateValuation valuation) {
		setName(name);
		this.valuation = valuation;
		collectedValuation = null;
	}

	public BookkeepedValuation collect(AGDLProsumerFactor parent) {
		//System.out.println("Collecting on node " + this);
		parent.numMessages++;
		List<BookkeepedValuation> ml = new ArrayList<BookkeepedValuation>(
				getNumNeighbors() - 1);
		List<Link> links = new ArrayList<Link>(getNumNeighbors() - 1);
		for (Factor f : getInNeighbors()) {
			AGDLProsumerFactor af = (AGDLProsumerFactor) f;
			if (af != parent) {
				// System.out.println("Calling collect on in neighbor "+af.getName());
				BookkeepedValuation val = af.collect(this);
				// System.out.println("Aggregating the result of collect on "+af.getName()
				// + " whose value is "+ val);
				ml.add(val);
				links.add(getLink(af));
			}
		}
		int numInNeighbors = ml.size();
		for (Factor f : getOutNeighbors()) {
			AGDLProsumerFactor af = (AGDLProsumerFactor) f;
			if (af != parent) {
				// System.out.println("Calling collect on out neighbor "+af.getName());
				BookkeepedValuation val = af.collect(this);
				// System.out.println("Aggregating the result of collect on "+af.getName()
				// + " whose value is "+ val);
				ml.add(val);
				links.add(getLink(af));
			}
		}
		List<Set<Integer>> domains = new ArrayList<Set<Integer>>();
		for (BookkeepedValuation v : ml) {
			domains.add(v.getKeys());
		}

		int capacity = 0;
		if (this != parent) {
			capacity = getLink(parent).capacity;
		}
		collectedValuation = new BookkeepedValuation();
		for (int k = -capacity; k <= capacity; k++) {
			// System.out.println("k=" + k);
			// Assignment a = new Assignment();
			// if (this!=parent) {
			// a.put(getLink(parent), k);
			// }
			IndexIterator indexIterator = new IndexIterator(domains);
			List<Integer> index;
			int s;
			double value;
			int x_i;
			while (indexIterator.hasNext()) {
				Assignment a = new Assignment();
				index = indexIterator.next();
				// System.out.println(index);
				s = 0;
				value = 0.0;
				for (int i = 0; i < numInNeighbors; i++) {
					x_i = index.get(i);
					s -= x_i;
					value += ml.get(i).getValue(x_i);
					a.put(links.get(i), x_i);
					// System.out.println("Setting "+ links.get(i) +
					// " to value "+x_i);
				}
				// System.out.println("xxxxx");
				for (int i = numInNeighbors; i < index.size(); i++) {
					x_i = index.get(i);
					s += x_i;
					value += ml.get(i).getValue(x_i);
					a.put(links.get(i), x_i);
					// System.out.println("Setting "+ links.get(i) +
					// " to value "+x_i);
				}
				// System.out.println("yyyy");
				if (valuation.containsKey(k - s)) {
					value += valuation.get(k - s);
					// System.out.println("Considering pair "+k+","+value);
					collectedValuation.consider(k, a, value);
				}
			}
		}
		if (this != parent) {
			if (getOutNeighbors().contains(parent)) {
				collectedValuation = collectedValuation.negate();
			}
		}
		collectedValuation.filter(capacity);
		// System.out.println("The collectedValuation on "+ getName() + " is "+
		// collectedValuation);
		if (this != parent) {
			BookkeepedValuation msg = collectedValuation
					.recode(getLink(parent));
			// System.out.println("The message from "+ getName() +
			// " to its parent is "+ msg);
			return msg;
		} else {
			return collectedValuation;
		}
	}

	public Assignment distribute(AGDLProsumerFactor parent, Assignment a) {
		parent.numMessages++;
		int value = 0;
		if (this != parent) {
			Link l = getLink(parent);
			value = a.get(l);
		}
		Assignment result = collectedValuation.getAssignmentWithValue(value);
		//System.out.println("Extended = " + result);
		for (Factor f : getNeighbors()) {
			if (f != parent) {
				Assignment fExtension = ((AGDLProsumerFactor) f).distribute(
						this, result);
				result = result.extend(fExtension);
			}
		}
		return result;
	}

}
