package seas2.ilp;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.glp_cpxcp;
import org.gnu.glpk.glp_iocp;
import org.gnu.glpk.glp_prob;

import seas2.core.Assignment;
import seas2.core.BaseSolver;
import seas2.core.Link;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.ilp.io.LpProblemExporter;

public class GLPKSolver extends BaseSolver implements Solver {

	public static final String suffix = new String("GLPK");

	public static final String inFile = new String("inFile");
	public static final String outFile = new String("outFile");
	public static final String eval = new String("eval" + suffix);

	@Override
	public Options timed_solve(Problem problem, Options m) {
		return solve(problem,m);
	}
	
	@Override
	public Options solve(Problem problem, Options m) {
		Options results = new Options();
		
		// Write file 
		GLPK.glp_java_set_numeric_locale("C");
		
		try {
			File temp = File.createTempFile(suffix, ".lp");
			String inF = temp.getAbsolutePath();
			Path inFPath = FileSystems.getDefault().getPath(inF);
			//Path inF = FileSystems.getDefault().getPath("problem.lp");
			//if (m.containsKey(inFile))
			//	inF = (Path) m.get(inFile);
			new LpProblemExporter().export(problem, inFPath);
			
			//System.out.println( GLPK.glp_version());
		
			// Load 
			glp_prob lp_problem;
			glp_cpxcp nuse = new glp_cpxcp();
			lp_problem = GLPK.glp_create_prob();
			int r = GLPK.glp_read_lp(lp_problem, nuse, inF);
			if (r == 0) {
				System.out.println("Loaded successfully ");
			}
			
	
			// Solve
			
			glp_iocp iocp = new glp_iocp();
			GLPK.glp_init_iocp(iocp);
			iocp.setPresolve(GLPKConstants.GLP_ON);
			long timeStart = System.currentTimeMillis();
			r = GLPK.glp_intopt(lp_problem, iocp);
			long timeEnd = System.currentTimeMillis();
			results.put(time, timeEnd-timeStart);
			
			// Get solution
			
			Assignment a = new Assignment();
			for (int i = 1; i <= GLPK.glp_get_num_cols(lp_problem); i++) {
				String varName = GLPK.glp_get_col_name(lp_problem,i);
				Link l = problem.findLink(varName);
				if (l != null) {
					int value = (int) Math.round(GLPK.glp_mip_col_val(lp_problem, i));
					a.put(l, value);
				}
			}
	
			results.put(Solver.solution, a);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return results;
	}

}
