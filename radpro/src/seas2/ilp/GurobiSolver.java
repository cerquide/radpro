package seas2.ilp;


import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBModel;
import gurobi.GRBVar;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import seas2.core.Assignment;
import seas2.core.BaseSolver;
import seas2.core.Link;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.ilp.io.LpProblemExporter;

public class GurobiSolver extends BaseSolver implements Solver {

	public static final String suffix = new String("GUROBI");

	public static final String inFile = new String("inFile");
	public static final String eval = new String("eval" + suffix);

	public class NullOutputStream extends OutputStream {
		  @Override
		  public void write(int b) throws IOException {
		  }
	}
	
	@Override
	public Options timed_solve(Problem problem, Options m) {
		return solve(problem,m);
	}
	
	@Override
	public Options solve(Problem problem, Options m) {
		Options results = new Options();
		try {
			// Write file
			File temp = File.createTempFile(suffix, ".lp");
			String inF = temp.getAbsolutePath();
			Path inFPath = FileSystems.getDefault().getPath(inF);
			new LpProblemExporter().export(problem, inFPath);
			
			// Load 
			GRBEnv env = new GRBEnv();
			env.set(GRB.IntParam.Aggregate, 0);
			env.set(GRB.IntParam.Threads, 1);
			env.set(GRB.DoubleParam.MIPGap, 0.0);
			env.set(GRB.DoubleParam.MIPGapAbs, 0.0);
			
			GRBModel model = new GRBModel(env,inF.toString());
			
			// Solve
			
			long timeStart = System.currentTimeMillis();
			model.optimize();
			long timeEnd = System.currentTimeMillis();
			results.put(time, timeEnd-timeStart);
			
			// Get solution
			
			Assignment a = new Assignment();
			for (Link l : problem.getLinks()) {
				GRBVar v = model.getVarByName(l.getVariableName());
				int value = (int) Math.round(v.get(GRB.DoubleAttr.X));
				a.put(l, value);
			}
			results.put(Solver.solution, a);
			//results.put(eval, model.getObjective());
			model.dispose();
			env.dispose();
		} catch(GRBException e) {
			System.out.println(e);
			return results;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}

}
