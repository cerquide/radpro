package seas2.ilp.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import seas2.core.Assignment;
import seas2.core.Link;
import seas2.maxsum.FactorGraph;

public final class FactorGraphExporter {
	public static void exportAssignmentToJSON(FactorGraph g, String path) {
		String s = "{ \"assignment\": [";
		int n = 0;
		Assignment assignment = g.decode();
		for (Map.Entry<Link,Integer> e : assignment.entrySet()) {
			s += "{\"variable:\"" + e.getKey().getVariableName() + ", " + "\"value\": " + e.getValue()	+ "}";
			if (++n < assignment.size())
				s += ",";
		}
		s += "]}";
		try {
			File file = new File(path);
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(s);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}