package seas2.ilp.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import seas2.core.Link;
import seas2.core.Participant;
import seas2.core.Problem;
import seas2.core.StateValuation;

public class LpProblemExporter {

	public void export(Problem problem, Path path) {
		System.out.println("Writing problem to file "+path.toString());
		try {
			BufferedWriter output = Files.newBufferedWriter(path,
					StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
			output.write("Maximize\n");
			output.write("\t");
			output.write("obj: ");
			objectiveFunction(output, problem);
			output.write("\nSubject to\n");
			onlyOneStateConstraints(output, problem);
			statesMatchWithNetFlows(output, problem);
			output.write("\nBounds\n");
			flowDoesNotExceedCapacity(output, problem);
			output.write("\nBinaries\n");
			writeBinaries(output, problem);
			output.write("\nGeneral\n");
			writeGenerals(output, problem);
			output.write("\nEnd\n");
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeBinaries(BufferedWriter output, Problem problem)
			throws IOException {
		for (Participant p : problem.getParticipants()) {
			for (int i = 0; i < p.getValuation().size(); i++) {
				output.write(" " + participantVar(p, i));
			}
			output.write("\n");
		}
	}

	private void writeGenerals(BufferedWriter output, Problem problem)
			throws IOException {
		for (Link l : problem.getLinks()) {
			output.write(" " + linkVar(l));
			output.write("\n");
		}
	}

	private void flowDoesNotExceedCapacity(BufferedWriter output,
			Problem problem) throws IOException {
		for (Link l : problem.getLinks()) {
			output.write(number(-l.capacity) + " <= " + linkVar(l) + " <= "
					+ number(l.capacity) + "\n");
		}
	}

	private void statesMatchWithNetFlows(BufferedWriter output, Problem problem)
			throws IOException {
		for (Participant p : problem.getParticipants()) {
			stateMatchesWithNetFlow(output, p);
		}
	}

	private void stateMatchesWithNetFlow(BufferedWriter output, Participant p)
			throws IOException {
		netFlow(output, p);
		int i = 0;
		for (Map.Entry<Integer, Double> e : p.getValuation().entrySet()) {
			double value = e.getKey();
			output.write(" ");
			//output.write((i == 0) ? number(-value) : term(-value));
			output.write(term(-value));
			output.write(" ");
			output.write(participantVar(p, i));
			i++;
		}
		output.write(" = 0");
		output.write("\n");
	}

	private void netFlow(BufferedWriter output, Participant p)
			throws IOException {
		int i = 0;
		for (Link l : p.getOutLinks()) {
			if (i != 0)
				output.write(" + ");
			output.write(linkVar(l));
			i++;
		}
		for (Link l : p.getInLinks()) {
			output.write(" - ");
			output.write(linkVar(l));
		}
	}

	private String linkVar(Link l) {
		return "y_" + l.source.getId() + "_" + l.dest.getId();
	}

	private void onlyOneStateConstraints(BufferedWriter output, Problem problem)
			throws IOException {
		for (Participant p : problem.getParticipants()) {
			onlyOneStateConstraint(output, p);
		}
	}

	private void onlyOneStateConstraint(BufferedWriter output, Participant p)
			throws IOException {
		for (int i = 0; i < p.getValuation().size(); i++) {
			if (i != 0) {
				output.write(" + ");
			}
			output.write(participantVar(p, i));
		}
		output.write(" = 1\n");
	}

	private void objectiveFunction(BufferedWriter output, Problem problem)
			throws IOException {
		int i = 0;
		for (Participant p : problem.getParticipants()) {
			exportParticipantTerm(output, p, (i == 0));
			output.write("\n");
			i++;
		}
	}

	private void exportParticipantTerm(BufferedWriter output, Participant p,
			boolean isFirst) throws IOException {
		StateValuation v = p.getValuation();
		int i = 0;
		for (Map.Entry<Integer, Double> e : v.entrySet()) {
			double value = e.getValue();
			value = roundZeros(value);
			if (value != 0.0) {
				output.write(" ");
				output.write(((i == 0) && isFirst) ? number(value) : term(value));
				output.write(" ");
				output.write(participantVar(p, i));
			}
			i++;
		}
	}

	private double roundZeros(double value) {
		if ((value < 1e-12) && (value > -1e-12)) {
			return 0.0;
		} else {
			return value;
		}
	}
	private String participantVar(Participant p, int index) {
		return "x_" + p.getId() + "_" + index;
	}

	private String term(double value) {
		if (value > 1e-12) {
			return "+" + value;
		} else {
			if (value < -1e-12) {
				return Double.toString(value);
			} else {
				return "+ 0.0";
			}
		}
	}

	private String number(double value) {
		return Double.toString(value);
	}

}
