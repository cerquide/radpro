package seas2.ilp;

import ilog.concert.IloException;
import ilog.concert.IloLPMatrix;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.DoubleParam;
import ilog.cplex.IloCplex.IntParam;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Iterator;

import seas2.core.Assignment;
import seas2.core.BaseSolver;
import seas2.core.Link;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.Solver;
import seas2.ilp.io.LpProblemExporter;

public class CPLEXSolver extends BaseSolver implements Solver {

	public static final String suffix = new String("CPLEX");

	public static final String inFile = new String("inFile");
	public static final String outFile = new String("outFile");
	//public static final String opt = new String("cplexOpt");
	public static final String eval = new String("eval" + suffix);
	//public static final String solution = new String("solution" + suffix);
	//public static final String time = new String("time" + suffix);

	public class NullOutputStream extends OutputStream {
		  @Override
		  public void write(int b) throws IOException {
		  }
	}
	
	@Override
	public Options timed_solve(Problem problem, Options m) {
		return solve(problem,m);
	}
	
	@Override
	public Options solve(Problem problem, Options m) {
		Options results = new Options();
		try {
			//Writing
			
			Path inF = FileSystems.getDefault().getPath(".", "problem.lp");

			if (m.containsKey(inFile))
				inF = (Path) m.get(inFile);
			else {
				File temp = File.createTempFile(suffix, ".lp");
				inF = FileSystems.getDefault().getPath(temp.getAbsolutePath());
			}
			
			new LpProblemExporter().export(problem, inF);
			Path outF = FileSystems.getDefault().getPath(".", "problem.sol");
			if (m.containsKey(outFile))
				outF = (Path) m.get(outFile);
			
			//Loading
			
			IloCplex cplex = new IloCplex();
			cplex.setOut(new NullOutputStream());
			cplex.importModel(inF.toString());
			
			// Solving 
			
			//CPLEX seems to have a bug with some of the problems generated. As suggested by IBM we can solve it fixing this variable to 0
			cplex.setParam(IntParam.AggInd, 0);
			//This was the former solution suggested by Marc Pujol:
			// cplex.setParam(BooleanParam.PreInd, false);
			
			// Since we want the solver to be exact, be set the MIP gaps to 0.
			cplex.setParam(DoubleParam.EpAGap, 0.0);
			cplex.setParam(DoubleParam.EpGap, 0.0);
			
			// We only use one thread
			cplex.setParam(IntParam.Threads, 1);
			long timeStart = System.currentTimeMillis();
			boolean solved = cplex.solve();
			long timeEnd = System.currentTimeMillis();
			results.put(time, timeEnd-timeStart);
			if (solved) {
				System.out.println("solved");
			} else {
				System.out.println("not solved");
			}
			
			// Getting solution
			
			Assignment a = new Assignment();
			@SuppressWarnings("rawtypes")
			Iterator it = cplex.getModel().iterator();
			it.next();
			IloLPMatrix mat = (IloLPMatrix) it.next();
			// System.out.println(mat);
			IloNumVar[] vars = mat.getNumVars();
			for (int i = 0; i < vars.length; i++) {
				String varName = vars[i].getName();
				Link l = problem.findLink(varName);
				if (l != null) {
					int value = (int) Math.round(cplex.getValue(vars[i]));
					a.put(l, value);
				}
			}
			results.put(Solver.solution, a);
			results.put(Solver.eval, problem.eval(a));
			results.put(CPLEXSolver.eval, cplex.getObjValue());
			cplex.writeSolutions(outF.toString());
			cplex.end();
		} catch (IloException e) {
			System.err.println("Concert exception caught: " + e);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return results;
	}

}
