package seas2.experiments;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;

import seas2.actiongdl.ActionGDLSolver;
import seas2.actiongdl.FastActionGDLSolver;
import seas2.core.Assignment;
import seas2.core.Options;
import seas2.core.Problem;
import seas2.core.io.AssignmentExporter;
import seas2.ilp.CPLEXSolver;
import seas2.ilp.GLPKSolver;
import seas2.ilp.GurobiSolver;
import seas2.nonlinear.io.JSONInstanceParser;

public class Solver {
	public static enum SolverType {//MAXSUM, FASTMAXSUM, 
		ACTIONGDL, FASTACTIONGDL, CPLEX, GUROBI, GLPK};

	public static void main(String[] args) throws IOException,
			InterruptedException {

		if (args.length < 3)
			throw new InvalidParameterException(
					Solver.class.getSimpleName()
							+ " usage is : java [-cp ...] "
							+ Solver.class
							+ "problem_file solver solution_file");
		
		SolverType st = SolverType.valueOf(args[1]);
		solve(args[0],st,args[2]);	
	}

	private static seas2.core.Solver getSolver(SolverType st) {
		switch(st) {
		case ACTIONGDL:
			return new ActionGDLSolver();
		case FASTACTIONGDL:
			return new FastActionGDLSolver();
		case CPLEX:
			return new CPLEXSolver();
		case GUROBI:
			return new GurobiSolver();
		case GLPK:
			return new GLPKSolver();
		default:
			return null;
		}
	}
	
	private static void solve(String infile, SolverType st, String outfile) {
		double gamma = 1.0;
		Problem problem = JSONInstanceParser.parse(new File(infile), gamma);
		Options result = new Options();
		Options o = new Options();
		seas2.core.Solver s = getSolver(st);
		result = s.timed_solve(problem, o);
		Assignment sol = (Assignment) result.get(seas2.core.Solver.solution);
		result.put(seas2.core.Solver.eval, problem.eval(sol));
		AssignmentExporter.exportAssignmentToJSON(sol, outfile + "assignment.json");
		AssignmentExporter.exportAssignmentToCSV(sol, outfile + "assignment.csv");
		result.saveToJSON(outfile);
	}
}
