#!/usr/bin/python

# Checks whether the values of the solutions found are equal
import matplotlib as mpl
mpl.use('Agg')
from experiment import *
from pprint import pprint
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
import matplotlib
import matplotlib.font_manager as fm

def convertSolverName(solver):
    if (str(solver) == "FASTACTIONGDL"):
        return r'\textsc{RadPro}'
    elif (str(solver) == "ACTIONGDL"):
        return r'\textsc{Acyclic-Solving}'
    else:
        return str(solver)

def limits(cap):
    if cap == 10:
        return [1,1e4]
    else:
        if cap == 100:
            return [5,1e6]

def getMedianAndQuartiles(x):
    # Assess median
    summary = x.median()["time"]
    dp = x.quantile(.05)["time"]
    up = x.quantile(.95)["time"]
    return (summary,summary-dp,up-summary)
            
def getMeanAndSem(x):
    # Assess mean
    summary = x.mean()
    
    #error = x.apply(pd.DataFrame.sem)
    # This seems to be present in pandas 0.15
    error = x.sem()
    #print "Error"
    #print error
    dp = (summary - error)
    #print type(dp),dp
    dp = dp["time"]
    
    #.drop('size',1).flatten()
    up = (summary + error)["time"]
    #["time"].flatten()
    #.drop('size',1).flatten()
    return (summary,error["time"],error["time"])
    
if __name__=="__main__":
    d = getDataFrame()
    d = d.select(lambda x: x in ['solver','size','capacity','time'], axis=1)
    # change font to fit with AAMAS font style: 
    # matplotlib.rcParams.update({'font.family': 'Arial'})
    plt.rcParams['text.latex.preamble']=[r"\usepackage{txfonts}\renewcommand{\familydefault}{\sfdefault}\usepackage{sansmath}\sansmath"]
    plt.rc('text', usetex=True)
    plt.rc('font', family='sans-serif')
    
    for capacity in conf["capacities"]:
        dcap=d[d.capacity==capacity].drop('capacity',1)
        plt.figure(figsize=(10,6))
        plt.grid()
        #print str(capacity)
        #print dcap
        
        marker = itertools.cycle(('.','*','v','^','o'))
        linestyle = itertools.cycle(('-','--', ':','-.'))
        for solver in conf["solvers"]:
            x = dcap[dcap.solver==solver].drop('solver',1).groupby('size')
            summary, dp, up = getMedianAndQuartiles(x)
            #summary, dp, up = getMeanAndSem(x)
            print "summary:",summary
            print "dp:",dp
            print "up:",up
            xs = np.array(summary.index)
            ys = np.array(summary).flatten()
            plt.errorbar(xs,ys, yerr =[dp,up],marker=marker.next(),markersize=10,linestyle=linestyle.next(),label=convertSolverName(solver))
        font_prop = fm.FontProperties(size=20)
        plt.yscale('log')
        plt.ylim(limits(capacity))
        plt.xlim([conf["sizes"][0],conf["sizes"][-1]])
        #plt.title("Runtime of the different algorithms (capacity="+str(capacity)+")")
        plt.xlabel('Number of prosumers',fontproperties=font_prop)
        plt.ylabel('Time in ms',fontproperties=font_prop)
        plt.legend(loc='best')#, title='Algorithm')
        plt.savefig("SolverSpeedComparisonForCapacity_"+str(capacity)+".pdf",bbox_inches='tight')
    #plt.show()

