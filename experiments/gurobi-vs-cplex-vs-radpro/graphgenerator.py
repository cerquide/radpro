import sys
import networkx as nx
import argparse
import matplotlib.pyplot as plt
import random
import decimal
import math
import numpy
from array import array
from networkx.utils import uniform_sequence, create_degree_sequence
from networkx.algorithms import bipartite
from os.path import basename
from os.path import splitext
from networkx.utils import powerlaw_sequence
from networkx.generators.degree_seq import degree_sequence_tree
from networkx.generators.random_graphs import random_powerlaw_tree_sequence

from networkx.readwrite import json_graph

# Default file suffix for json files
JSON_FILE = sys.stdout
# Default file suffix for dot files
DOT_FILE = 'graph.dot'
# Default file suffix for pdf files
PDF_FILE = 'graph.pdf'
# Default network size (# prosumers)
N = 100
# Default image size
CANVAS_SIZE = 100.0
# Default random generator seed
SEED = 1
# Default ratio #producers/#consumers
PC_RATIO = 0.3
# Default average price per unit
MEAN_PRICE = 1
# Default variance over the price per unit
VARIANCE_PRICE = 0.5
# Default average unit production capacity for producers
MEAN_PRODUCTION = 100
# Default variance over unit production capacity for producers
VARIANCE_PRODUCTION = 5
# Default average unit consumption need for consumers
MEAN_CONSUMPTION = 10
# Default variance over unit consumption for consumers
VARIANCE_CONSUMPTION = 5
# Default number of hops to built prosumers' neighborhood
HOPS = 2
# Default delta_q parameter to gerenate TRR
DELTA_Q = 1.0
# Default network topology
TOPOLOGY='tree'
# Default node size
NODE_SIZE = 2
# Are images exported?
IMAGES = False

BRANCHING = 4

DEPTH = 3
 
CHOICES = ['tree', 'mesh', 'smallworld','complete','balanced','powerlaw','star','geometric']

MAXB = 100

DECIMAL = "0.0001"

def main(argv):
    """
    Main program to generate a prosumer network.
    This network is based on a RRT tree to which some edges are added depending on (i) the state of each nodes and (ii) the size of the neighborhood (HOPS).
    """
    global N, JSON_FILE, TOPOLOGY, HOPS, SEED, PC_RATIO, CANVAS_SIZE, DELTA_Q, NODE_SIZE, IMAGES, BRANCHING, DEPTH, MEAN_PRODUCTION, MEAN_CONSUMPTION
    # Parse command arguments:
    parser = argparse.ArgumentParser(description='Generate a SEAS scenario configuration file.')
    parser.add_argument('size', metavar='N', type=int,
                        help='the size of the network (# participants)')
    parser.add_argument('-o','--outfile',type=argparse.FileType('w'),default=JSON_FILE,help='the file into which the configuration is written (default: stdout)')
    parser.add_argument('-t','--topology', metavar='TOPOLOGY',default='tree', choices=CHOICES,help='the topology of the network connecting agents (default: tree, others not implemented...)')
    parser.add_argument('-d','--distance', default=HOPS, metavar="HOPS", help="the number of hop to create neighborhoods (default: 1)",type=int)
    parser.add_argument('-s','--seed', default=SEED, metavar="SEED", help="the seed to initialize pseudo-random number generator (default: 1)")
    parser.add_argument('-r','--ratio', default=PC_RATIO, type=float, metavar="PC_RATIO", help="the ratio #producers/#consumers (default: 0.3)")
    parser.add_argument('-mp','--meanp', default=MEAN_PRODUCTION, type=float, metavar="MEAN_PRODUCTION", help="the average number of produced units per participant (default: 10)")
    parser.add_argument('-mc','--meanc', default=MEAN_CONSUMPTION, type=float, metavar="MEAN_CONSUMPTION", help="the average number of consumed units per participant (default: 10)")
    parser.add_argument('-c','--canvas', default=CANVAS_SIZE, metavar="CANVAS_SIZE", type=float, help="the size of the generated images (default: 100.0)")
    parser.add_argument('-q','--deltaq', default=DELTA_Q,type=float, metavar="DELTA_Q", help="the delta_q parameter to gerenate TRRs (default: 1.0)")
    parser.add_argument('-n','--node', default=NODE_SIZE,type=int, metavar="NODE_SIZE", help="the size of node in exported figures (default: 2)")
    parser.add_argument('-i','--images', help="switch on image export", action='store_true',dest="images")
    parser.add_argument('-b','--branching', default=BRANCHING, metavar="BRANCHING", help="the branching factor for balanced trees (default: 4)",type=int)
    parser.add_argument('-H','--depth', default=DEPTH, metavar="DEPTH", help="the depth for balanced trees (default: 3)",type=int)
    parser.add_argument('-f','--fixed', action='store_true')
    args = parser.parse_args()
    
    if (args.node == NODE_SIZE):
        node_size = CANVAS_SIZE**2/args.size
    else:
        node_size = args.node
    generator = GraphGenerator(args.size, args.outfile, args.topology, args.distance, args.seed, args.ratio, args.canvas, args.deltaq, node_size, args.images,args.branching,args.depth,fixed=args.fixed)
    generator.run()

class GraphGenerator:
    
    def __init__(self, size, outfile= JSON_FILE, topology= TOPOLOGY, distance= HOPS, seed= SEED, ratio= PC_RATIO, canvas= CANVAS_SIZE,deltaq= DELTA_Q, node= NODE_SIZE, images= IMAGES, branching= BRANCHING, depth= DEPTH, meanp=  MEAN_PRODUCTION, meanc=  MEAN_CONSUMPTION, fixed= False):
        self.size = size
        self.outfile = outfile
        self.topology = topology
        self.distance = distance
        self.seed = seed
        self.ratio = ratio
        self.canvas = canvas
        self.deltaq = deltaq
        self.node = node
        self.images = images
        self.branching = branching
        self.depth = depth
        self.meanp = meanp
        self.meanc = meanc
        self.fixed = fixed
        if self.topology == 'balanced':
            nodeCount = 0
            for d in range(1,self.depth+1):
                nodeCount += self.branching ** d
            self.size = nodeCount+1
        print self.fixed
         
    def run(self):
        random.seed(self.seed)
        # Create new graph:
        G=nx.Graph()
        # Generate the attributes of the participants
        participants = self.generate_participants(G);
        prefix = splitext(self.outfile.name)[0]
        # Generate a graph depending on the chosen type (here a tree):
        if self.topology == 'mesh':
            #TODO : generate_mesh(G,participants)
            if self.images: self.export_graph(G,prefix+'_mesh_')
        elif self.topology == 'smallworld':
            #TODO : generate_smallworld(G,participants)
            if self.images: self.export_graph(G,prefix+'_smallworld_')
        elif self.topology == 'balanced':
            self.generate_balanced(G,participants)
            if self.images: self.export_graph(G,prefix+'_balanced_')
        elif self.topology == 'complete':
            #TODO : generate_complete(G,participants)
            if self.images: self.export_graph(G,prefix+'_complete_')
        elif self.topology == 'powerlaw':
            self.generate_random_powerlaw_tree(G,participants)
            #self.generate_random_decreasing_uniform_law_tree(G,participants)
            if self.images: self.export_graph(G,prefix+'_powerlaw_')
        elif self.topology == 'geometric':
            self.generate_random_geometric_tree(G,participants)
            #self.generate_random_decreasing_uniform_law_tree(G,participants)
            if self.images: self.export_graph(G,prefix+'_geometric_')
        elif self.topology == 'star':
            self.generate_star_tree(G,participants)
            #self.generate_random_decreasing_uniform_law_tree(G,participants)
            if self.images: self.export_graph(G,prefix+'_star_')
        else:
            self.generate_tree(G,participants)
            if self.images: self.export_graph(G,prefix+'_tree_')
        # Create extra edges dependending on the neighborhood length
        # print nx.degree_histogram(G)
        generate_neighbor_edges(G,self.distance)
        if self.images: self.export_graph(G,prefix+'_hops_')
        # Suppress edge between p-p and c-c, to create a bipartite graph:
        # transform_bipartite(G)
        # if self.images: self.export_graph(G,prefix+'_bipartite_',True)
        # if self.images: self.export_graph(G,prefix+'_columns_',True,True)
        # Output the bipartite graph as a json file
        #f = open(JSON_FILE, 'w+')
        self.outfile.write (json_graph.dumps(G))
        G2 = nx.Graph()
        G2.add_edges_from(G.edges())
        G2.add_nodes_from(G.nodes(data=False))
        nx.readwrite.write_gexf(G2, prefix+".graphml")
        #print nx.average_neighbor_degree(G)

    def export_graph(self,G,prefix,show_disconnected=True,columns=False):
        """
        Write the current state of the network G as a PDF file and a DOT file.
        """
        plt.clf()
        top_nodes=G.subgraph( [n for n,attrdict in G.node.items() if attrdict['cost'] >= 0 and len(G.neighbors(n)) > 0]).nodes()
        bottom_nodes=G.subgraph( [n for n,attrdict in G.node.items() if attrdict['cost'] < 0 and len(G.neighbors(n)) > 0]).nodes()
        disconnected_top_nodes = G.subgraph( [n for n,attrdict in G.node.items() if attrdict['cost'] >= 0 and len(G.neighbors(n)) <= 0]).nodes()
        disconnected_bottom_nodes = G.subgraph( [n for n,attrdict in G.node.items() if attrdict['cost'] < 0 and len(G.neighbors(n)) <= 0]).nodes()
        if columns:
            pos = columns_layout(G,self.canvas,self.deltaq)
        #elif self.topology == 'star':
            #pos=nx.spectral_layout(G)
        #elif self.topology == 'powerlaw':
            #pos=nx.spectral_layout(G)
        else:
            pos = {}
            for node in G.nodes(data=True):
                pos[node[0]] = (node[1]['x'],node[1]['y'])
        #else : 
        #    pos=nx.graphviz_layout(G,prog='neato')
        nx.set_node_attributes(G,'pos',pos)
        nx.draw_networkx_nodes(G, pos,node_shape='.',with_labels=False,node_size=self.node,alpha=0.7,linewidths=0.1,nodelist=bottom_nodes,node_color='g')
        nx.draw_networkx_nodes(G, pos,node_shape='.',with_labels=False,node_size=self.node,alpha=0.7,linewidths=0.1,nodelist=top_nodes,node_color='r')
        if show_disconnected:
            nx.draw_networkx_nodes(G, pos,node_shape='.',with_labels=False,node_size=self.node,alpha=0.2,linewidths=0.1,nodelist=disconnected_bottom_nodes,node_color='g')
            nx.draw_networkx_nodes(G, pos,node_shape='.',with_labels=False,node_size=self.node,alpha=0.2,linewidths=0.1,nodelist=disconnected_top_nodes,node_color='r')
        nx.draw_networkx_edges(G,pos,width=min(0.3,self.node),alpha=0.5)
        plt.axis('off')
        plt.axes().set_aspect(1)
        plt.savefig(prefix+PDF_FILE,aspect='equal')
        nx.write_dot(G,prefix+DOT_FILE)

    def generate_participants(self,G):
        """
        Generate states for all the participants.
        """
        N = self.size
        participants = []
        if N<2:
            raise Exception('Size of the problem is incorrect (should be > 1)')
        else:
            for i in range(N):
                participants.append(self.generate_participant(G,i))
        return participants
  
    def generate_participant(self,G,i):
        """
        Generate the state (min, max, cost) for a single participant.
        This generation is based on the following parameters:
        - PC_RATIO
        - MEAN_PRODUCTION
        - MEAN_CONSUMPTION
        - VARIANCE_PRODUCTION
        - VARIANCE_CONSUMPTION
        - MEAN_PRICE
        - VARIANCE_PRICE
        """
        mult = -1 if (random.random() > self.ratio) else 1
        mpc = self.meanp if mult < 0 else self.meanc
        if self.fixed:
            vpc = 0
        else:
            vpc = self.meanp/2 if mult < 0 else self.meanc/2
        vmax = max(1,int(random.gauss(mpc,vpc)))
        if self.fixed:
            vmin = 1
        else:
            vmin = random.randint(1,vmax);
        #vcost = mult * float(decimal.Decimal(random.gauss(MEAN_PRICE,VARIANCE_PRICE)).quantize(decimal.Decimal(DECIMAL), rounding=decimal.ROUND_UP))
        vcost = mult * float(decimal.Decimal(random.gauss(MEAN_PRICE,VARIANCE_PRICE)))
        return {'id': i, 'min': vmin, 'max': vmax, 'cost': vcost};

    def generate_scale_free_tree(self,G,participants):
        G = nx.scale_free_graph(self.size)
        G = G.to_undirected()
        for u,v,d in G.edges(data=True):
            d['weight'] = 1
        G = nx.minimum_spanning_tree(G)
        #print G.nodes(data=True)
        for i in range(0,self.size):
            #G.node[i]['id'] = participants[i]['id']
            G.node[i]['min'] = participants[i]['min']
            G.node[i]['max'] = participants[i]['max']
            G.node[i]['cost'] = participants[i]['cost']
            G.node[i]['bipartite'] = 0 if G.node[i]['cost'] < float(0) else 1
        print G.nodes(data=True)
        print G.edges(data=True)
        print nx.degree_histogram(G)

    def generate_random_powerlaw_tree(self,G,participants):
        # generate degree sequence
        dseq = nx.utils.create_degree_sequence(self.size,random_powerlaw_tree_sequence,gamma=3,tries=1000*self.size)
        dseq.sort(reverse=True)
        k = 0
        while dseq[k] >= MAXB:
            diff = dseq[k] - MAXB
            for i in range(1,diff+1):
                dseq[k+i] = dseq[k+i] + 1
            dseq[k] = MAXB
            k = k+1
        #print dseq
        G = degree_sequence_tree(dseq, G)
        for i in range(0,self.size):
            #G.node[i]['id'] = participants[i]['id']
            G.node[i]['min'] = participants[i]['min']
            G.node[i]['max'] = participants[i]['max']
            G.node[i]['cost'] = participants[i]['cost']
            G.node[i]['bipartite'] = 0 if G.node[i]['cost'] < float(0) else 1
            G.node[i]['x'] = 0
            G.node[i]['y'] = 0
        #print nx.degree_histogram(G)

    def generate_random_geometric_tree(self,G,participants):
        # generate degree sequence
        dseq = numpy.random.geometric(0.5,self.size).tolist()
        while(len(dseq)-float(sum(dseq))/2 != 1):
            dseq = numpy.random.geometric(0.5,self.size).tolist()
        print(dseq)
        dseq.sort(reverse=True)
        k = 0
        while dseq[k] >= MAXB:
            diff = dseq[k] - MAXB
            for i in range(1,diff+1):
                dseq[k+i] = dseq[k+i] + 1
            dseq[k] = MAXB
            k = k+1
        print dseq
        G = degree_sequence_tree(dseq, G)
        for i in range(0,self.size):
            #G.node[i]['id'] = participants[i]['id']
            G.node[i]['min'] = participants[i]['min']
            G.node[i]['max'] = participants[i]['max']
            G.node[i]['cost'] = participants[i]['cost']
            G.node[i]['bipartite'] = 0 if G.node[i]['cost'] < float(0) else 1
            G.node[i]['x'] = 0
            G.node[i]['y'] = 0
        print nx.degree_histogram(G)

    def generate_star_tree(self,G,participants):
        # generate degree sequence
        G = nx.star_graph(self.size-1, G)
        for i in range(0,self.size):
            #G.node[i]['id'] = participants[i]['id']
            G.node[i]['min'] = participants[i]['min']
            G.node[i]['max'] = participants[i]['max']
            G.node[i]['cost'] = participants[i]['cost']
            G.node[i]['bipartite'] = 0 if G.node[i]['cost'] < float(0) else 1
            G.node[i]['x'] = 0
            G.node[i]['y'] = 0
        print nx.degree_histogram(G)

    """def generate_random_decreasing_uniform_law_tree(self,G,participants):
        # generate degree sequence
        dseq = []
        count = 0
        for i in range(0,self.size):
            r = random.uniform(1,2*(self.size-1)-count)
            count = count + r
            dseq.append(r)
        
        G = degree_sequence_tree(dseq, G)
        for i in range(0,self.size):
            #G.node[i]['id'] = participants[i]['id']
            G.node[i]['min'] = participants[i]['min']
            G.node[i]['max'] = participants[i]['max']
            G.node[i]['cost'] = participants[i]['cost']
            G.node[i]['bipartite'] = 0 if G.node[i]['cost'] < float(0) else 1
            G.node[i]['x'] = 0
            G.node[i]['y'] = 0
        print nx.degree_histogram(G)"""

    def generate_balanced(self,G,participants):
        G = nx.balanced_tree(self.branching, self.depth, G)
        #print G.nodes(data=True)
        x = 0
        y = 0
        for i in range(0,self.size):
            #G.node[i]['id'] = participants[i]['id']
            G.node[i]['min'] = participants[i]['min']
            G.node[i]['max'] = participants[i]['max']
            G.node[i]['cost'] = participants[i]['cost']
            G.node[i]['bipartite'] = 0 if G.node[i]['cost'] < float(0) else 1
            G.node[i]['x'] = float(x * 10.0)
            G.node[i]['y'] = float(y * 10.0)
            #G.node[i]['pos'] = (float(x*10.0),float(y*10.0))
            if x < self.branching :
                x = x * y + 1
            else : 
                x = 0
                if y < self.depth :
                   y = y + 1
                else :
                   y = 0
        #print G.nodes(data=True)

    def generate_tree(self,G,participants):
        """
        Generate a RRT (see http://msl.cs.uiuc.edu/rrt/about.html) as the underlying structure of the energy network.
        """
        cparticipants = []
        cparticipants.extend(participants)
        qinit=(self.canvas/2,self.canvas/2)
        pinit = cparticipants.pop()
        G.add_node(pinit['id'],min=pinit['min'], max=pinit['max'], shape='point', bipartite=0 if pinit['cost'] < float(0) else 1,cost=pinit['cost'],x=qinit[0],y=qinit[1],pos=(qinit[0],qinit[1]))
        for i in range(0,len(cparticipants)):
            iter = 0
            qrand = (random.uniform(0,self.canvas),random.uniform(0,self.canvas))
            nnear = nearest_neighbor(qrand,G)
            qnear = (nnear[1]['x'],nnear[1]['y'])
            qnew = new_neighbor(qnear,qrand,self.deltaq)
            pnew = cparticipants.pop()
            G.add_node(pnew['id'],min=pnew['min'], max=pnew['max'], shape='point', bipartite=0 if pnew['cost'] < float(0) else 1,cost=pnew['cost'],x=qnew[0],y=qnew[1],pos=(qnew[0],qnew[1]))
            G.add_edge(pnew['id'],nnear[0])

def distance(x1,y1,x2,y2):
    """
    Compute th euclidian distance between two points.
    """
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)

def nearest_neighbor(qrand,G):
    """
    Return the nearest node to a given pair of coordinates (x,y).
    """
    nmin = G.nodes(data=True)[0]
    xnmin = nmin[1]['x']
    ynmin = nmin[1]['y']
    x1 = qrand[0]
    y1 = qrand[1]
    min = distance(qrand[0],qrand[1],xnmin,ynmin)
    for n in G.nodes(data=True):
        x2 = n[1]['x']
        y2 = n[1]['y']
        d = distance(x1,y1,x2,y2)
        if (d < min):
            nmin = n
            min = d
    return nmin

def new_neighbor(q1,q2,d):
    """
    Return the position (x,y) when moving from q1 in direction of q2 for a distance d.
    """
    qnx = q2[0] - q1[0]
    qny = q2[1] - q1[1]
    theta = math.atan2(qny,qnx)
    return (q1[0] + d * math.cos(theta), q1[1] + d * math.sin(theta))

def generate_neighbor_edges(G,hops):
    """
    Generate all the new transitional edges for determining the neighbor (HOPS given).
    """
    if hops > 1:
        neighborhood = {}
        for node in G.nodes():
            neighborhood[node] = []
            nearest = nx.single_source_shortest_path_length(G,node,hops)
            for n,d in nearest.iteritems():
                if (d>1):
                    neighborhood[node].append(n)
        for k,v in neighborhood.iteritems():
            for n in v:
                G.add_edge(k,n)
            
def transform_bipartite(G):
    """
    Removes all the edges p-p and c-c. Only p-c edges remain => bipartite graph.
    """
    pos = {}
    for node in G.nodes(data=True):
        remove_invalid_edges(G,node)
    #for node in G.nodes():
    #    if len(G.neighbors(node)) == 0:
    #        G.remove_node(node)
        
def remove_invalid_edges(G,node):
    """
    Removes all the edges p-p and c-c from the node.
    """
    for n in G.neighbors(node[0]):
        if (node[1]['bipartite'] == G.nodes(data=True)[n][1]['bipartite']):
            G.remove_edge(node[0],n)
            
def columns_layout(G,deltax,deltay):
    """
    Determine a graphical layout for the bipartite graph.
    """
    pos = {}
    top = 0
    bottom = 0
    for n in G.nodes(data=True):
        if (n[1]['cost'] < 0):
            pos[n[0]] = (float(deltax/4),float(bottom))
            bottom += 2.5*deltay
        else:
            pos[n[0]] = (float(3*deltax/4),float(top))
            top += 2.5*deltay
    return pos
    
if __name__ == "__main__":
    main(sys.argv[1:])
