#!/usr/bin/python
import os.path
import os
import json
import lockfile
import subprocess
from pprint import pprint
import pandas as pd
import graphgenerator
import hashlib 

def iterateCapacityDirs(conf):
    path = conf["problem_path"]
    for topology in conf["topologies"]:
        #print "====== Generating experiments for topology " + str(topology) + " ====="
        patht = os.path.join(path ,"topology_"+str(topology))
        for pcratio in conf["pcratios"]:
            #print "====== Generating experiments for pcratio " + str(pcratio) + " ====="
            pathp = os.path.join(patht ,"pcratio_"+ str(pcratio))
            for capacity in conf["capacities"]:
                #print "====== Generating experiments for capacity " + str(capacity) + " ====="
                pathc = os.path.join(pathp, "capacity_"+ str(capacity))
                yield {"path":pathc,"topology":topology,"pcratio":pcratio,"capacity":capacity}

def iterateDirs(conf):
    for currentConf in iterateCapacityDirs(conf):
        newConf = currentConf.copy()
        for size in conf["sizes"]:
            newConf["path"] = os.path.join(currentConf["path"], "size_"+ str(size))
            #print "====== Generating experiments for size " + str(size) + " ====== "
            newConf["size"] = size
            yield newConf
            #yield {"path":paths,"topology":topology,"pcratio":pcratio,"capacity":capacity,"size":size}

def iterateProblems(conf):
    for currentConf in iterateDirs(conf):
        newConf = currentConf.copy()
        for j in conf["repeats"]:
            newConf["repetition"]=j
            newConf["problem_file"] = os.path.join(currentConf["path"],'instance_'+ str(j) +'.json')
            yield newConf

def iterateSolverAndProblem(conf):
    for solver in conf["solvers"]:
        for currentConf in iterateProblems(conf):
            appendSolver(currentConf,solver)
            yield currentConf

def appendSolver(currentConf,solver):
    currentConf["solver"] = solver
    currentConf["solution_file"] = currentConf["problem_file"]+"_"+str(solver)+".sol"
    currentConf["status_file"] = currentConf["problem_file"]+"_"+str(solver)+".status"
    
def getResultData(currentConf,solver=None):
    if solver!=None:
        appendSolver(currentConf,solver)
    json_data =     open(currentConf["solution_file"])
    data = json.load(json_data)
    json_data.close()
    return data

def getDataFrame():
    l = []
    for currentConf in iterateSolverAndProblem(conf):
        data = getResultData(currentConf)
        data.update(currentConf)
        l.append(data)
    print "Data loaded"
    return pd.DataFrame(l)

def initDatas():
    datas = dict.fromkeys(conf["topologies"])

    for currentConf in iterateProblems(conf):
        topology = currentConf["topology"]
        if datas[topology] is None:
            datas[topology] = dict.fromkeys(conf["pcratios"]) 
        pcratio = currentConf["pcratio"]
        if datas[topology][pcratio] is None:
            datas[topology][pcratio] = dict.fromkeys(conf["capacities"])    
        capacity = currentConf["capacity"]
        if datas[topology][pcratio][capacity] is None:
            datas[topology][pcratio][capacity] = dict.fromkeys(conf["sizes"])
        size = currentConf["size"]
        if datas[topology][pcratio][capacity][size] is None:
            datas[topology][pcratio][capacity][size] = dict.fromkeys(conf["repeats"])
        repetition = currentConf["repetition"]
        if datas[topology][pcratio][capacity][size][repetition] is None:
            datas[topology][pcratio][capacity][size][repetition] = dict.fromkeys(conf["solvers"])
        for solver in conf["solvers"]:
            datas[topology][pcratio][capacity][size][repetition][solver] = getResultData(currentConf,solver)
    return datas
    
def generateProblems(conf):
    for currentConf in iterateDirs(conf):
        try:
            os.makedirs(currentConf["path"])
            print "...Directory created: "+ currentConf["path"]
        except OSError:
            if not os.path.isdir(currentConf["path"]):
                raise
    for currentConf in iterateProblems(conf):
        if not os.path.isfile(currentConf["problem_file"]):
            print "Generating problem " + currentConf["problem_file"]
            json_file = open(currentConf["problem_file"], 'w')
            generator = graphgenerator.GraphGenerator(currentConf["size"], outfile = json_file, seed = currentConf["repetition"], topology=currentConf["topology"], distance = 0, ratio=currentConf["pcratio"], meanp=currentConf["capacity"], meanc=currentConf["capacity"])
            generator.run()
            print "\t...JSON instance file created: " + json_file.name
            json_file.close()
    print "Problem generation finished"

def solve(currentConf):
    # java -Djava.library.path=$CPLEX_LIB -cp $MAXSUM_CLASSPATH $MAXSUM_EXPERIMENTS $MIN $MAX $STEP $REPETITIONS $GAMMA FASTACTIONGDL ACTIONGDL CPLEX > /dev/null
    print "Solving problem " + currentConf["problem_file"] + " with solver "+ str(currentConf["solver"])
    #print myenv["CLASSPATH"]
    print myenv["LIBRARYPATH"]
    exitValue = subprocess.call(["java", "-Djava.library.path="+myenv["LIBRARYPATH"], "-cp", myenv["CLASSPATH"], myenv["JAVASOLVER"], currentConf["problem_file"] , currentConf["solver"], currentConf["solution_file"]])
    lock= getLock(currentConf)
    with lock:
        status_file = open(currentConf["status_file"],"w")
        if exitValue==0:
            status_file.write("solved")
        else:
            status_file.write("failed")
        status_file.close()

def getLock(conf):
    h = hashlib.new('ripemd160')
    h.update(conf["problem_file"])
    fileName = "/tmp/"+h.hexdigest()
    #Caveat! They recently changed the name of the class to LockFile. Keeping the old one instead.
    return lockfile.FileLock(fileName)

def trySolve(currentConf):
    #print currentConf["problem_file"]
    lock = getLock(currentConf)
    #print "I am going to acquire lock"
    lock.acquire()
    #print "Lock acquired"
    shouldSolve = False
    if not os.path.isfile(currentConf["status_file"]):
        status_file = open(currentConf["status_file"],"w")
        status_file.write("being solved")
        status_file.close()
        lock.release()
        shouldSolve = True
    else:
        status_file = open(currentConf["status_file"],"r")
        status = status_file.readline()
        status_file.close()
        if (status=="failed"):
            status_file = open(currentConf["status_file"],"w")
            status_file.write("being solved")
            status_file.close()
            shouldSolve = True
        lock.release()
    if shouldSolve:
        #print "Solving it"
        solve(currentConf)
        #print "Solved"
    else:
        print "Already solved"

def solveProblems(conf, myenv):
    for currentConf in iterateSolverAndProblem(conf):
        trySolve(currentConf)
    print "Problem solving finished"
        
#    experiment = open('experiment.json')
#    conf = json.load(experiment)
#    pprint(conf)
#    json_data.close()


def getConfExample():
    conf={}
    conf["topologies"] = ["geometric"]
    conf["pcratios"] = [0.1]
    conf["capacities"] = [10, 100]
    conf["sizes"] = range(2,203,50)
    #conf["sizes"].append(250)
    #conf["sizes"].extend(range(300,2001,100))

    conf["repeats"] = range(10)
    conf["solvers"] = ["GUROBI","FASTACTIONGDL","CPLEX"]#,"GLPK"]
    conf["problem_path"] = "."
    return conf

conf = getConfExample()

def getConfPaper():
    conf={}
    conf["topologies"] = ["geometric"]
    conf["pcratios"] = [0.1]
    conf["capacities"] = [10, 100]
    conf["sizes"] = range(2,203,50)
    conf["sizes"].append(250)
    conf["sizes"].extend(range(300,2001,100))

    conf["repeats"] = range(100)
    conf["solvers"] = ["FASTACTIONGDL","CPLEX","GUROBI"]#,"GLPK"]
    conf["problem_path"] = "."
    return conf

def getEnv():
    #envFile = open('env.json')
    #env = json.load(envFile)
    #pprint(env)
    #envFile.close()
    cplexHome = os.environ["CPLEX_HOME"]
    gurobiHome = os.environ["GUROBI_HOME"]
    radproClasspath = os.environ["RADPRO_CLASSPATH"]
    gsonPath = "/usr/share/java/gson.jar"
    myenv={}    
    myenv["CPLEX_JAR"] = cplexHome + "/lib/cplex.jar"
    myenv["CPLEX_LIB"] = cplexHome + "/bin/x86-64_linux/"
    myenv["GUROBI_JAR"]= gurobiHome + "/lib/gurobi.jar"
    myenv["GUROBI_LIB"] = gurobiHome +"/lib/"
    #myenv["GLPK_JAR"]= "/usr/share/java/glpk-java.jar"
    #myenv["GLPK_LIB"] = "/usr/lib/x86_64-linux-gnu/jni"
    myenv["SEASPATH"] = radproClasspath
    myenv["CLASSPATH"] = myenv["CPLEX_JAR"] + ":" + myenv["GUROBI_JAR"]#":"+myenv["GLPK_JAR"]+
    myenv["CLASSPATH"] = myenv["CLASSPATH"] + ":" + radproClasspath+"/:" + gsonPath
    print "CLASSPATH="
    print myenv["CLASSPATH"] 
    myenv["LIBRARYPATH"] =  myenv["GUROBI_LIB"]
    #+ ":" + myenv["CPLEX_LIB"] #+ ":"+myenv["GLPK_LIB"]
    print "LIBRARYPATH="
    print myenv["LIBRARYPATH"] 
    myenv["JAVASOLVER"] = "seas2.experiments.Solver"
    return myenv
    
if __name__=="__main__":
    generateProblems(conf)
    myenv = getEnv()
    solveProblems(conf, myenv)

