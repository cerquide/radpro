RadPro AAMAS 2015 experiments
===========================

This repository contains the code needed to replicate the experiments in the paper "Designing a Marketplace for the Trading and Distribution of Energy In the Smart Grid" at AAMAS 2105.

The code for the solvers is inside the RadPro directory. The suggested procedure to reproduce the results uses a virtual machine so that it has a minimal  impact on the machine of the scientist willing to reproduce the results. This process is as much as possible automated (except for the installation of Gurobi and CPLEX, due to licensing issues). Alternatively, the scientist willing to reproduce the results can do so on their own machine by taking care of the software installation himself.

The only requirements on the machine used to reproduce the experiments are:

   1. To have a running version of ansible.
   2. To have a running version of VirtualBox.
   3. To have a running version of git.

In a Ubuntu machine these three requirements are satisfied by running the 
commands
```
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible virtualbox git
```

The next step is to clone this git repository in your preferred directory (we will use `radprodir` in our case)
```
cd radprodir
git clone git@bitbucket.org:cerquide/radpro.git
```

After that you power up a new virtual machine which will have most of the software installed by simply doing
```
vagrant up
```
Take a coffee and a good book since this will take a while...

The next step is installing CPLEX and Gurobi on the virtual machine that has just being created. We could not automate this since a license is required. 

Installing Gurobi
-----------------------
If you are in academia you can get a free Gurobi academic license. Check out  [this link](http://www.gurobi.com/academia/for-universities)
You should download the version for Linux64 from http://user.gurobi.com/download/gurobi-optimizer and store the file into `radprodir`

```
vagrant ssh
sudo tar xvfz /vagrant/gurobi6.0.2_linux64.tar.gz -C /opt
echo "export GUROBI_HOME=/opt/gurobi602/linux64" >> .bashrc
echo "export PATH=\${GUROBI_HOME}/bin:\${PATH}" >> .bashrc
echo "export LD_LIBRARY_PATH=\${GUROBI_HOME}/lib:\${LD_LIBRARY_PATH}" >> .bashrc
source .bashrc
grbgetkey <put_your_key_id_here>
```
In the last step you will be prompted for a directory. Just press enter to use the default.

Installing CPLEX
-----------------------
If you are in academia you can get a free CPLEX academic license by joining the IBM academic initiative. Check out  [this link](http://www-304.ibm.com/ibm/university/academic/pub/page/ban_prescriptive_analytics)

``` 
Similar procedure for CPLEX 
```

Compiling RadPro
------------------------
Just do
```
vagrant ssh
cd /vagrant/radpro
ant compile
echo "export RADPRO_CLASSPATH=/vagrant/radpro/bin" >> .bashrc
exit
```

Running the Radpro vs. CPLEX vs Gurobi comparison
------------------------------------------------------------------------
```
vagrant ssh
cd /vagrant/experiments/gurobi-vs-cplex-vs-radpro
python experiment.py
```
This would be the right time for another coffee... Several hours after
```
python plot.py
exit
evince *.pdf
```